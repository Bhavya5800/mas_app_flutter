import 'dart:convert';

import 'package:f_app/DailyTask.dart';
import 'package:f_app/Trip.dart';
import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

final key = new GlobalKey<ScaffoldState>();

class LecLabDetails extends StatefulWidget {
  final String title;
  final int slotNo;

  LecLabDetails(this.title, this.slotNo);

  @override
  _MyLecLabDetails createState() =>
      new _MyLecLabDetails(this.title, this.slotNo);
}

class _MyLecLabDetails extends State<LecLabDetails> {
  final String title;
  final int slotNo;

  _MyLecLabDetails(this.title, this.slotNo);
  SharedPreferences sharedPreferences;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List<String> _sems = <String>['<Select Any>', '1', '2', '3', '4', '5', '6', '7', '8'];
  List<String> _divs = <String>[
    '<Select Any>',
    'DIV : A',
    'DIV : B',
    'DIV : C',
    'DIV : D',
    'DIV : E',
    'DIV : F',
    'DIV : G',
    'DIV : H'
  ];
  String _sem = '<Select Any>';
  String _div = '<Select Any>';
  LecLabDetailsClass labDetailsClass = new LecLabDetailsClass();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: key,
      resizeToAvoidBottomInset: false,
      appBar: new AppBar(
        title: new Text("Daily Task"),
      ),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
              key: _formKey,
              autovalidate: true,
              child: new ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                children: <Widget>[
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.subject),
                      hintText: 'Enter Subject Name',
                      labelText: 'Subject Name',
                    ),
                    inputFormatters: [new LengthLimitingTextInputFormatter(30)],
                    validator: (val) =>
                        val.isEmpty ? 'Subject Name is required' : null,
                    onSaved: (val) => labDetailsClass.subjectName = val,
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.supervisor_account),
                      hintText: 'Enter Department',
                      labelText: 'Department Name',
                    ),
                    inputFormatters: [new LengthLimitingTextInputFormatter(30)],
                    validator: (val) =>
                        val.isEmpty ? 'Department Name is required' : null,
                    onSaved: (val) => labDetailsClass.departmentName = val,
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.supervisor_account),
                      hintText: 'Enter total student',
                      labelText: 'Total Student',
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
                    validator: (val) =>
                        val.isEmpty ? 'Total Student is required' : null,
                    onSaved: (val) => labDetailsClass.totCount = int.parse(val),
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.supervised_user_circle),
                      hintText: 'Enter present student',
                      labelText: 'Present Student',
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
                    validator: (val) =>
                        val.isEmpty ? 'Present Student is required' : null,
                    onSaved: (val) =>
                        labDetailsClass.totPresent = int.parse(val),
                  ),
                  new FormField(
                    builder: (FormFieldState state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.add_box),
                          labelText: 'Semester',
                        ),
                        isEmpty: _sem == '<Select Any>',
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            value: _sem,
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                labDetailsClass.semId = int.parse(newValue);
                                _sem = newValue;
                                state.didChange(newValue);
                              });
                            },
                            items: _sems.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                    validator: (val) {
                      return val != '<Select Any>' ? null : 'Please select a semester';
                    },
                  ),
                  new FormField(
                    builder: (FormFieldState state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.ac_unit),
                          labelText: 'Division',
                        ),
                        isEmpty: _div == '<Select Any>',
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            value: _div,
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                labDetailsClass.divison = newValue;
                                _div = newValue;
                                state.didChange(newValue);
                              });
                            },
                            items: _divs.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                    validator: (val) {
                      return val != '<Select Any>' ? null : 'Please select a division';
                    },
                  ),
                  new Container(
                      padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                      child: new RaisedButton(
                        child: const Text('Submit'),
                        onPressed: () {
                          fillDailyTask(context);
                        },
                      )),
                ],
              ))),
    );
  }

//  Future<bool> _onWillPop() async {
//    return (await showDialog(
//      context: context,
//      builder: (context) => new AlertDialog(
//        title: new Text('Are you sure?'),
//        content: new Text('Do you want to submit task'),
//        actions: <Widget>[
//          new FlatButton(
//            onPressed: () => Navigator.of(context).pop(false),
//            child: new Text('No'),
//          ),
//          new FlatButton(
//            onPressed: () => fillDailyTask(context),
//            child: new Text('Yes'),
//          ),
//        ],
//      ),
//    )) ??
//        false;
//  }

  Future<String> fillDailyTask(context) async {
    final FormState form = _formKey.currentState;
    sharedPreferences = await SharedPreferences.getInstance();

    form.save();

    print("Present Total: " + labDetailsClass.totCount.toString());
    print("Present Sem: " + labDetailsClass.semId.toString());
    print("Present Title: " + labDetailsClass.subjectName.toString());
    print("Present subject: " + labDetailsClass.divison.toString());
    if (labDetailsClass.totPresent > labDetailsClass.totCount) {
      key.currentState.showSnackBar(SnackBar(
          content: Text("Present Student not greater than total student!")));
      return "Success";
    }
  try {
    var datas = {
      "facultyId": sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
      "batchName": labDetailsClass.departmentName,
      "dayId": 1,
      "lecOrLab": title,
      "semId": labDetailsClass.semId,
      "slotId": slotNo,
      "subjectName":
      labDetailsClass.subjectName + "\n" + labDetailsClass.divison,
      "totCount": labDetailsClass.totCount,
      "totPresent": labDetailsClass.totPresent
    };
    var response = await http.post(
      Uri.parse(CommonConstants.HOST_URL + CommonConstants.STD_ATT_TOT_API),
      body: json.encode(datas),
      headers: CommonConstants.HOST_HEADER,
    );
    var datass = jsonDecode(response.body);
    print(datass.toString());
  } on Exception {}
  finally {
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new DailyTaskView(),
    );

    Navigator.of(context).pop();
    Navigator.of(context).pop();
    Navigator.of(context).push(route);
  }
    return "Success";
  }
}
