import 'dart:convert';
import 'dart:ui' as ui;

import 'package:f_app/PrincipalStaffNotice.dart';
import 'package:f_app/StaffNotice.dart';
import 'package:f_app/Trip.dart';
import 'package:f_app/utils/CommonConstants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bloc.dart';

class MyGlobals {
  GlobalKey _scaffoldKey;
  MyGlobals() {
    _scaffoldKey = GlobalKey();
  }
  GlobalKey get scaffoldKey => _scaffoldKey;
}

MyGlobals myGlobals = new MyGlobals();

class StaffNoticeDetailsView extends StatelessWidget {
  final  data;

  String _mySelection;
  StaffNoticeDetailsView(this.data);
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  var otherData;
  SharedPreferences sharedPreferences;
//  Future<String> getData() async {
//    sharedPreferences = await SharedPreferences.getInstance();
//
//    var response = await http.get(
//      Uri.parse(CommonConstants.HOST_URL +
//          CommonConstants.LEACE_STATUS_API +
//          data.memoId.toString()),
//      headers: CommonConstants.HOST_HEADER,
//    );
//
//    otherData = jsonDecode(response.body);
//
//    // final dlist = ddmap["content"] as List;
//    print("Data : " + otherData.toString());
//
//    response = await http.get(
//      Uri.parse(CommonConstants.HOST_URL + CommonConstants.LEAVETYPE_API),
//      headers: CommonConstants.HOST_HEADER,
//    );
//
//    var datas = jsonDecode(response.body);
//    final Typedlist = datas as List;
//
//    Map mp = {"id": 99, "name": "By Principal"};
//    leaveDutyType.add(mp);
//    for (var names in Typedlist) {
//      final dmap = names as Map;
//      // final dlist = ddmap["content"] as List;
//      //  print("Data : " + Typedlist.toString());
//      int id = dmap["id"];
//      String name = dmap["name"];
//      Map mp = {"id": id, "name": name};
//      leaveDutyType.add(mp);
//    }
//    //print("Datass : " + leaveDutyType.toString());
//
//    return "Success";
//  }

  List<Map> leaveDutyType = [];

  @override
  Widget build(BuildContext context) {
    // getData();
    final bloc = Bloc();
    print("Received : " + data["id"].toString());
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;

    final String imgUrl =
        'https://rememberingplaces.com/wp-content/themes/listingpro/assets/images/admin/avtar.jpg';
    final key = new GlobalKey<ScaffoldState>();
    return new Stack(
      children: <Widget>[
        new Container(
          color: Colors.blue,
        ),
        new Image.network(
          imgUrl,
          fit: BoxFit.fill,
        ),
        new BackdropFilter(
            filter: new ui.ImageFilter.blur(
              sigmaX: 6.0,
              sigmaY: 6.0,
            ),
            child: new Container(
              decoration: BoxDecoration(
                color: Colors.blue.withOpacity(0.9),
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
              ),
            )),
        new Scaffold(
          resizeToAvoidBottomInset: false,
          key: key,
          appBar: new AppBar(
            title: new Text("Staff Notice Details"),
            centerTitle: false,
            elevation: 0.0,
            backgroundColor: Colors.transparent,
          ),
          backgroundColor: Colors.blue.withOpacity(0.9),
          body: SingleChildScrollView(
            child: new Center(
              child: new Column(
                children: <Widget>[
                  new SizedBox(
                    height: _height / 12,
                  ),
                  new CircleAvatar(
                    radius: _width < _height ? _width / 4 : _height / 4,
                    backgroundImage: NetworkImage(imgUrl),
                  ),
                  new SizedBox(
                    height: _height / 25.0,
                  ),

                  new Text(
                    data["lotCreationDate"] != null
                        ? 'Notice Date : ' + data["lotCreationDate"]
                        : 'Notice Date :' + "-",
                    style: new TextStyle(fontSize: 18, color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    data["memoCount"] != null
                        ? 'Notce Number : ' + data["memoCount"].toString()
                        : 'Notce Number : ' + "-",
                    style: new TextStyle(fontSize: 18, color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    data["isSubmitted"] == true
                        ? 'Status : ' + "Replied"
                        : 'Status : ' + "Pending",
                    style: new TextStyle(fontSize: 18, color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    data["memoReason"] != null
                        ? 'Reason : ' + data["memoReason"]
                        : 'Reason : ' + "-",
                    style: new TextStyle(
                        fontSize: 16,
                        color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Padding(
                    padding: new EdgeInsets.only(
                        left: _width / 8, right: _width / 8),
                    child: new FlatButton(
                      onPressed: () {
                        var reason = StreamBuilder<String>(
                          stream: bloc.password,
                          builder: (context, snapshot) => TextField(
                            onChanged: bloc.passwordChanged,
                            keyboardType: TextInputType.text,
                            maxLines: 3,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: "Enter Notice Reason",
                                labelText: "Notice Reason",
                                errorText: snapshot.error),
                            controller: _passwordController,
                          ),
                        );

                        if (data["isSubmitted"] != true) {
                          Alert(
                              context: context,
                              title: "Reply for Notice",
                              content: reason,
                              buttons: [
                                DialogButton(
                                  child: Text(
                                    "Submit",
                                    style: TextStyle(
                                        color: Colors.green, fontSize: 20),
                                  ),
                                  onPressed: () {
                                    getEoutAtt(
                                        context, _passwordController.text);
                                    Navigator.of(context, rootNavigator: true)
                                        .pop();
                                  },
                                  width: 120,
                                ),
                              ],
                              closeFunction: () {
                                print("Calling this..");
                                Navigator.of(context, rootNavigator: true)
                                    .pop();
                              }).show();
                        } else {
                          key.currentState.showSnackBar(new SnackBar(
                            content: new Text("Action : " + data["memoReason"]),
                          ));
                        }

//                        showDialog(
//                            context: myGlobals.scaffoldKey.currentContext,
//                            barrierDismissible: false,
//                            child: new AlertDialog(
//                              title: new Text(
//                                " Endorse Notice ",
//                                style: TextStyle(
//                                    color: Colors.red,
//                                    fontSize: 15.0,
//                                    fontWeight: FontWeight.bold,
//                                    fontFamily: 'serif'),
//                              ),
//                              actions: <Widget>[
//                                ButtonBar(
//                                  children: <Widget>[
//                                    FlatButton(
//                                      child: Text(
//                                        'Submit',
//                                        style: TextStyle(
//                                            color: Colors.white,
//                                            fontSize: 15.0,
//                                            fontWeight: FontWeight.bold,
//                                            fontFamily: 'serif'),
//                                      ),
//                                      onPressed: () {
//                                        Navigator.of(context,
//                                                rootNavigator: true)
//                                            .pop('dialog');
//                                        getEoutAtt(context, _passwordController.text);
//                                      },
//                                      color: Colors.red,
//                                    ),
//                                  ],
//                                ),
//                              ],
//                            ));
                      },
                      child: new Container(
                          child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Icon(Icons.dock),
                          new SizedBox(
                            width: _width / 30,
                          ),
                          new Text(data["isSubmitted"] == true
                              ?  "Replied"
                              : "Pending",)
                        ],
                      )),
                      color: Colors.blue[50],
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  static var _passwordController = new TextEditingController();
  Future<String> getEoutAtt(context, String reason) async {
    try {
      var dataInfo = {
        "id": data["id"],
        "isSubmitted": true,
        "memoReason": reason,
      };
      var response = await http.post(
        Uri.parse(
            CommonConstants.HOST_URL + CommonConstants.MEMO_UPDATE_BYFAC_API),
        body: json.encode(dataInfo),
        headers: CommonConstants.HOST_HEADER,
      );
      print("Display Response : " + response.toString());
    } on Exception catch (_) {
      print('never reached');
    } finally {
      var route = new MaterialPageRoute(
        builder: (BuildContext context) => new StaffNotice(),
      );
      Navigator.of(context).pop();
      Navigator.of(context).push(route);
    }

    return "Success";
  }

//  Future<bool> _onWillPop(context,String reason) async {
//    showDialog(
//      context: context,
//      builder: (context) => new AlertDialog(
//        title: new Text('Are you sure?'),
//        content: new Text('Do you want to endorsed notice reply'),
//        actions: <Widget>[
//          new FlatButton(
//            onPressed: () => Navigator.of(context).pop(false),
//            child: new Text('No'),
//          ),
//          new FlatButton(
//            onPressed: () => getEoutAtt(context,reason),
//            child: new Text('Yes'),
//          ),
//        ],
//      ),
//    );
//  }

}
