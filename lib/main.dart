import 'dart:developer';

import 'package:f_app/login.dart';
import 'package:f_app/pages/home_screen.dart';
import 'package:f_app/pages/intro_screen.dart';
import 'package:f_app/pages/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

var routes = <String, WidgetBuilder>{
  "/home": (BuildContext context) => HomeScreen(),
  "/intro": (BuildContext context) => IntroScreen(),
  "/login": (BuildContext context) => HomePage(),
//  "/dashboard": (BuildContext context) => PageTwo(null),
//  "/attendanceSummary": (BuildContext context) => AttendanceSummary(),
//  "/dailyTask": (BuildContext context) => DailyTaskView(),
//  "/chnagePassword": (BuildContext context) => ChangePassword(),
//  "/staffNotice": (BuildContext context) => StaffNotice(),
//  "/principalNotice": (BuildContext context) => PrincipalStaffNotice(),
//  "/markAttendance": (BuildContext context) => HomeView(),
//  "/editAttendance": (BuildContext context) => HomeEditView(),
};
String spEmail = "";
String spPassword = "";
SharedPreferences sharedPreferences;
checkSharedPrefernce() async {
  sharedPreferences = await SharedPreferences.getInstance();
  spEmail = sharedPreferences.getString("email");
  spPassword = sharedPreferences.getString("password");
  if (spEmail == null) {
    spEmail = "";
  }
  if (spPassword == null) {
    spPassword = "";
  }
  log("Data: load sharedPreferance");
}

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  checkSharedPrefernce();
  runApp(new MaterialApp(
      theme: ThemeData(
          primaryColor: Colors.indigoAccent, accentColor: Colors.black12),
      debugShowCheckedModeBanner: true,
      home: SplashScreen(),
      routes: routes));
}
