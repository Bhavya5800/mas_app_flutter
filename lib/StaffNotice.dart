import 'dart:convert';

import 'package:f_app/StaffNoticeDetailsView.dart';
import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'Trip.dart';

class StaffNotice extends StatefulWidget {
  //   final List<Trip> tripsList = [
  //    Trip("Bhavya shah", DateTime.now(), DateTime.now(), 200, "C.E"),
  //    Trip("Hemant Joshi", DateTime.now(), DateTime.now(), 450, "Civil"),
  //    Trip("Dhaval Sathvara", DateTime.now(), DateTime.now(), 900, "Admin"),
  //    Trip("Parth Modi", DateTime.now(), DateTime.now(), 170, "I.C"),
  //    Trip("Tushar Raval", DateTime.now(), DateTime.now(), 180, "C.E"),
  //  ];

  @override
  _MyStaffNotice createState() => _MyStaffNotice();
}

class _MyStaffNotice extends State<StaffNotice> {
  final List<FacultyNotice> facultyList = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  // List<Map> status = [];

  Future<List<FacultyNotice>> getData() async {
    sharedPreferences = await SharedPreferences.getInstance();

    var response = await http.get(
      Uri.parse(CommonConstants.HOST_URL +
          CommonConstants.FAC_MEMO_API +
          sharedPreferences
              .getInt(CommonSharedPreferance.FACULTY_ID)
              .toString()),
      headers: CommonConstants.HOST_HEADER,
    );

    var data = jsonDecode(response.body);
    var dlist = data as List;
    // final dlist = ddmap["content"] as List;
    print("New Data : " + data.toString());

    int cnt = 1;
    for (var name in dlist) {
      var dmap = name as Map;
      var arr = dmap["lotCreationDate"].toString().split("-");
      String memoDate = arr[2] + "-" + arr[1] + "-" + arr[0];
      bool facStatus = false;
      int memoId = dmap["id"];
      int memoCount = dmap["memoCount"];
      String dMsg = "Pending By Staff";
      facStatus = dmap["isSubmitted"];
      if (facStatus == null) {
        facStatus = false;
      }
      facultyList.add(FacultyNotice(memoDate, facStatus, memoCount, memoId));
      cnt += 1;
    }

    return facultyList;
  }

  @override
  Widget build(BuildContext context) {
    //getData();
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white70,
      appBar: AppBar(
        title: Text("Staff Notice Details"),
      ),
      body: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            return snapshot.data != null
                ? buildTripCard(snapshot.data)
                : Center(child: CircularProgressIndicator());
          }),
    );
//      Container(
//
//      child: new ListView.builder(
//          itemCount: tripsList.length,
//          itemBuilder: (BuildContext context, int index) =>
//              buildTripCard(context, index)),
//      color: Colors.white70,
//      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
//    );
  }

  Widget buildTripCard(List<FacultyNotice> faculty) {
    if (faculty.length == 0) {
      return new Container(
          child: Center(
        child: Text(
          "No Data Found..",
          style: new TextStyle(fontSize: 22.0),
        ),
      ));
    } else {
      return new Container(

        child: ListView.builder(
            itemCount: faculty.length,
            padding: const EdgeInsets.all(2.0),
            itemBuilder: (context, position) {
              return Card(
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, bottom: 5.0),
                        child: Row(children: <Widget>[
                          Text(
                            "Notice Date : " + faculty[position].noticeDate,
                            style: new TextStyle(fontSize: 17.0),
                          ),
                          Spacer(),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, bottom: 5.0),
                        child: Row(children: <Widget>[
                          Text(
                            "Notice No : " +
                                faculty[position].noticeNo.toString(),
                            style: new TextStyle(fontSize: 17.0),
                          ),
                          Spacer(),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 1.0, bottom: 1.0),
                        child: Row(
                          children: <Widget>[
                            Spacer(),
                            ButtonBar(
                              children: <Widget>[
                                FlatButton(
                                  child: Text(
                                    faculty[position].noticeStatus == true
                                        ? 'Replied'
                                        : 'Pending',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'serif'),
                                  ),
                                  onPressed: () {
//                                    Alert(
//                                        context: context,
//                                        title: "Edit Staff Action",
//                                        desc:
//                                            "By Principal Marked attendance updated to present, leave, duty for : ",
//                                        buttons: [
//                                          DialogButton(
//                                            child: Text(
//                                              "OK",
//                                              style: TextStyle(
//                                                  color: Colors.white,
//                                                  fontSize: 20),
//                                            ),
//                                            onPressed: () {
//                                              Navigator.of(context,
//                                                      rootNavigator: true)
//                                                  .pop();
//                                            },
//                                            width: 120,
//                                          )
//                                        ],
//                                        closeFunction: () {
//                                          print("Calling this..");
//                                          Navigator.of(context,
//                                                  rootNavigator: true)
//                                              .pop();
//                                        }).show();

                                   // print("Clicked!");
                                    show(context, faculty[position]);
                                  },
                                  color: Colors.indigo,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
      );
    }
  }

 Future<String> show(BuildContext context, FacultyNotice data) async {
    print(data.memoId.toString());
    var response = await http.get(
      Uri.parse(CommonConstants.HOST_URL +
          CommonConstants.MEMO_API +
          data.memoId.toString()),
      headers: CommonConstants.HOST_HEADER,
    );

    var memoData = jsonDecode(response.body);
    print("Get Data : " + memoData.toString());
    print("Clicked Process!");
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new StaffNoticeDetailsView(memoData),
    );
    Navigator.of(context).pop();
    Navigator.of(context).push(route);
 return "Success";
  }
}
