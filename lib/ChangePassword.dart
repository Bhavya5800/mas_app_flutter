import 'dart:async';
import 'dart:convert';
import 'dart:convert' show jsonDecode;
import 'dart:developer';

import 'package:connectivity/connectivity.dart';
import 'package:f_app/bloc.dart';
import 'package:f_app/pages/home_screen.dart';
import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ios_network_info/ios_network_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyChangePassword extends StatefulWidget {
  @override
  _MyChangePasswordState createState() => new _MyChangePasswordState();
}

class _MyChangePasswordState extends State<MyChangePassword> {
  changeThePage(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => HomeScreen()));
  }

  SharedPreferences sharedPreferences;
  static var _passwordController = new TextEditingController();
  static var _newPasswordController = new TextEditingController();
  static var _newConfPasswordController = new TextEditingController();
  static var data;

  static var _isSecured = true;

  //List data;
  String _bssid = 'Unknown BSSID';
  String _ssid = 'Unknown SSID';

  fetchAll() {
    fetchBssid();
    fetchSsid();
  }

  fetchBssid() async {
    try {
      _bssid = await IosNetworkInfo.bssid;
    } on Exception {
      _bssid = 'Failed to get WiFi BSSID.';
    }
  }

  fetchSsid() async {
    try {
      _ssid = await IosNetworkInfo.ssid;
    } on Exception {
      _ssid = 'Failed to get WiFi SSID.';
    }
  }

  /*Future<String> apiRequest(String url, Map jsonMap) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(jsonMap)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    return reply;
  }
  getData() async {
    String url =
        'http://www.dheemati.in/springWebAPI/api/student/login/';
    var datas = {
      'data':{
        "email": "jyoti@test.com",
        "password": "12345",
        "bssid": "72:bb:e9:b2:60:be"
      },
    };

    print(await apiRequest(url, datas));
  }*/

  String spEmail = "";
  String spPassword = "";

  int count = 1;
  @override
  Widget build(BuildContext context) {
    final bloc = Bloc();

    Future<String> getData(
        String pwd, String newPwd, String newConfirm, context) async {
      sharedPreferences = await SharedPreferences.getInstance();

      var datas = {
        "fId": sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
        "oldPassword": _passwordController.text,
        "newPassword": _newPasswordController.text,
        "retypePassword": _newConfPasswordController.text,
      };
      var response = await http.post(
        Uri.parse(CommonConstants.HOST_URL + CommonConstants.CHANGE_PWD_API),
        body: json.encode(datas),
        headers: CommonConstants.HOST_HEADER,
      );

      var connectivityResult = await (Connectivity().checkConnectivity());
      print(connectivityResult.toString());

      // I am connected to a wifi network.

      var data = jsonDecode(response.body);

      print(data.toString());

      var cMsg = data as Map;
      if (cMsg["message"] == "Password updated successfully") {
        Navigator.of(context).pop();
      } else {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text(cMsg["message"])));
      }
//      if (data["faId"] > 0) {
//        builder:
//        (BuildContext context) => new PageTwo(data);
//      } else {
//        Scaffold.of(context)
//            .showSnackBar(SnackBar(content: Text(data["message"])));
//      }
      setState(() {
        count = 1;
      });
      return "Success";
    }

    var passwords = StreamBuilder<String>(
      stream: bloc.password,
      builder: (context, snapshot) => TextFormField(
        onChanged: bloc.passwordChanged,
        keyboardType: TextInputType.text,
        obscureText: true,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: "enter old password",
            labelText: "Old Password",
            errorText: snapshot.error),
        controller: _passwordController,
      ),
    );
    var NewPasswords = StreamBuilder<String>(
      stream: bloc.password,
      builder: (context, snapshot) => TextFormField(
        onChanged: bloc.passwordChanged,
        keyboardType: TextInputType.text,
        obscureText: true,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: "enter new password",
            labelText: "New Password",
            errorText: snapshot.error),
        controller: _newPasswordController,
      ),
    );
    var NewConfPasswords = StreamBuilder<String>(
      stream: bloc.password,
      builder: (context, snapshot) => TextFormField(
        onChanged: bloc.passwordChanged,
        keyboardType: TextInputType.text,
        obscureText: true,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: "enter new confirm password",
            labelText: "New Confirm Password",
            errorText: snapshot.error),
        controller: _newConfPasswordController,
      ),
    );
    /*********************Alert Dialog Pseudo******************************/

    List<Widget> children =
        new List.generate(count, (int i) => new InputWidget(i));
    log("Data Msg : $count");
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text("MAS Change Password"),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              StreamBuilder<String>(
                  builder: (context, snapshot) => Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            StreamBuilder<String>(
                              builder: (context, snapshot) => Image(
                                height: 100.0,
                                width: 100.0,
                                semanticLabel: "Mobile Attendance System",
                                image: AssetImage("assets/image/seals.png"),
                              ),
                            ),
                            StreamBuilder<String>(
                              builder: (context, snapshot) => Text(
                                "Education Department\nGovernment of Gujarat\n(Mobile Attendance System)",
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'serif'),
                              ),
                            ),
                          ])),
              StreamBuilder<String>(
                builder: (context, snapshot) => Image(
                  height: 100.0,
                  width: 100.0,
                  semanticLabel: "Mobile Attendance System",
                  image: AssetImage("assets/image/proffes.png"),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              StreamBuilder<String>(
                builder: (context, snapshot) => Text(
                  "Mobile Attendance System \n Change Password",
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              passwords,
              SizedBox(
                height: 20.0,
              ),
              NewPasswords,
              SizedBox(
                height: 20.0,
              ),
              NewConfPasswords,
              SizedBox(
                height: 20.0,
              ),
              SizedBox(
                height: 20.0,
              ),
              StreamBuilder<bool>(
                stream: bloc.submitCheck,
                builder: (context, snapshot) => MaterialButton(
                  color: Colors.amber,
                  height: 50.0,
                  onPressed: () {
                    //Perform some action
                    if (_passwordController.text.trim().length < 4) {
                      Scaffold.of(context).showSnackBar(
                          SnackBar(content: Text('Invalid OLD Passowrd!')));
                    } else if (_newPasswordController.text.trim().length < 4) {
                      Scaffold.of(context).showSnackBar(
                          SnackBar(content: Text('Invalid NEW Passowrd!')));
                    } else if (_newConfPasswordController.text.trim().length <
                        4) {
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text('Invalid Confirm NEW Passowrd!')));
                    } else if (_newPasswordController.text !=
                        _newConfPasswordController.text) {
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text(
                              'Both New and Confirm Passowrd Not Matched!')));
                    } else {
                      getData(
                          _passwordController.text,
                          _newPasswordController.text,
                          _newConfPasswordController.text,
                          context);
                      setState(() {
                        count = 2;
                      });
                    }

                    //  print(result);
                  },
                  child: Text("Change Password"),
                  minWidth: 400.0,
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              children.elementAt(0),
              SizedBox(
                height: 20.0,
              ),
              StreamBuilder<String>(
                builder: (context, snapshot) => Text("DTE Gujarat @ 2019"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class InputWidget extends StatelessWidget {
  final int index;
  static int count = 0;

  InputWidget(this.index);

  @override
  Widget build(BuildContext context) {
    count += 1;
    log("Data Index : $index");
    if (index == 2)
      return new CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
      );
    else
      return new CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(Colors.transparent),
      );
  }
}
