import 'dart:convert';
import 'dart:ui' as ui;

import 'package:f_app/home_edit.dart';
import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditUserAttendance extends StatefulWidget {
  final data;
  //   final List<Trip> tripsList = [
  //    Trip("Bhavya shah", DateTime.now(), DateTime.now(), 200, "C.E"),
  //    Trip("Hemant Joshi", DateTime.now(), DateTime.now(), 450, "Civil"),
  //    Trip("Dhaval Sathvara", DateTime.now(), DateTime.now(), 900, "Admin"),
  //    Trip("Parth Modi", DateTime.now(), DateTime.now(), 170, "I.C"),
  //    Trip("Tushar Raval", DateTime.now(), DateTime.now(), 180, "C.E"),
  //  ];
  EditUserAttendance(this.data);
  @override
  _MyEditUserAttendance createState() => _MyEditUserAttendance(data);
}

class _MyEditUserAttendance extends State<EditUserAttendance> {
  final data;

  String _mySelection;
  String _mySelectionString;
  _MyEditUserAttendance(this.data);

  var otherData;
  SharedPreferences sharedPreferences;
  Future<String> getData() async {
    sharedPreferences = await SharedPreferences.getInstance();

    var response = await http.get(
      Uri.parse(CommonConstants.HOST_URL +
          CommonConstants.LEACE_STATUS_API +
          data[0].toString()),
      headers: CommonConstants.HOST_HEADER,
    );

    otherData = jsonDecode(response.body);

    // final dlist = ddmap["content"] as List;
    print("Data : " + otherData.toString());

    response = await http.get(
      Uri.parse(CommonConstants.HOST_URL + CommonConstants.LEAVETYPE_API),
      headers: CommonConstants.HOST_HEADER,
    );

    var datas = jsonDecode(response.body);
    final Typedlist = datas as List;

    Map mp = {"id": 99, "name": "By Principal"};
    leaveDutyType.add(mp);
    for (var names in Typedlist) {
      final dmap = names as Map;
      // final dlist = ddmap["content"] as List;
      //  print("Data : " + Typedlist.toString());
      int id = dmap["id"];
      String name = dmap["name"];
      Map mp = {"id": id, "name": name};
      leaveDutyType.add(mp);
    }
    //print("Datass : " + leaveDutyType.toString());

    return "Success";
  }

  List<Map> leaveDutyType = [];

  String getSelectedValue(int value) {
    String message = "<<Select data>>";
    for (Map test in leaveDutyType) {
      if (test["id"] == value) {
        message = test["name"];
        break;
      }
    }
    return message;
  }

  @override
  Widget build(BuildContext context) {
    getData();
    if (_mySelection != null) {}
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;

    final String imgUrl =
        'https://rememberingplaces.com/wp-content/themes/listingpro/assets/images/admin/avtar.jpg';

    return new Stack(
      children: <Widget>[
        new Container(
          color: Colors.blue,
        ),
        new Image.network(
          imgUrl,
          fit: BoxFit.fill,
        ),
        new BackdropFilter(
            filter: new ui.ImageFilter.blur(
              sigmaX: 6.0,
              sigmaY: 6.0,
            ),
            child: new Container(
              decoration: BoxDecoration(
                color: Colors.blue.withOpacity(0.9),
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
              ),
            )),
        new Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: new AppBar(
            title: new Text("Edit User Attendance"),
            centerTitle: false,
            elevation: 0.0,
            backgroundColor: Colors.transparent,
          ),
          backgroundColor: Colors.blue.withOpacity(0.9),
          body: SingleChildScrollView(
              child: new Center(
            child: new Column(
              children: <Widget>[
                new SizedBox(
                  height: _height / 12,
                ),
                new CircleAvatar(
                  radius: _width < _height ? _width / 4 : _height / 4,
                  backgroundImage: NetworkImage(imgUrl),
                ),
                new SizedBox(
                  height: _height / 25.0,
                ),
                new Text(
                  'Staff Name :' + data[1],
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white),
                ),
                new Padding(
                    padding: new EdgeInsets.only(
                        top: _height / 30,
                        left: _width / 8,
                        right: _width / 8)),
                new Text(
                  'Department :' + data[2],
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white),
                ),
                new Padding(
                    padding: new EdgeInsets.only(
                        top: _height / 30,
                        left: _width / 8,
                        right: _width / 8)),
                new Text(
                  'Status : ' + data[3],
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white),
                ),
                new Padding(
                    padding: new EdgeInsets.only(
                        top: _height / 30,
                        left: _width / 8,
                        right: _width / 8)),
                new Text(
                  'Leave Type : ',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white),
                ),
                new Padding(
                    padding: new EdgeInsets.only(
                        top: _height / 30,
                        left: _width / 8,
                        right: _width / 8)),
                new Text(
                  'Leave Description : ',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white),
                ),
                new Divider(
                  height: _height / 30,
                  color: Colors.white,
                ),
                new Row(
                  children: <Widget>[
                    rowCell('Staff', 'Role'),
                    rowCell('-', 'Designation'),
                  ],
                ),
                new Divider(height: _height / 30, color: Colors.white),
                new Padding(
                  padding:
                      new EdgeInsets.only(left: _width / 8, right: _width / 8),
                  child: new FlatButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          child: new AlertDialog(
                            title: new Text(
                              "Edit Attendance!",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'serif'),
                            ),
                            content: Text(
                                "Note:\n\n 1. From ${otherData['leaveDesc']} status ${otherData['leaveType']} as past data cannot be modified.\n 2. From today onwards new effect will be considered."),
                            actions: <Widget>[
                              ButtonBar(
                                children: <Widget>[
                                  FlatButton(
                                    child: Text(
                                      'Edit Staff',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'serif'),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context, rootNavigator: true)
                                          .pop('dialog');

                                      showDialog(
                                        context: context,
                                        barrierDismissible: false,
                                        child: new AlertDialog(
                                          title: new Text(
                                            "Staff Action",
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'serif'),
                                          ),
                                          content: DropdownButton<String>(
                                            isDense: true,
                                            isExpanded: true,
                                            hint: new Text("<< Select >>"),
                                            value: _mySelection != null
                                                ? _mySelection
                                                : null,
                                            onChanged: (String newValue) {
                                              Navigator.of(context,
                                                      rootNavigator: true)
                                                  .pop('dialog');

//                                                setState(() {
//                                                  _mySelection = newValue;
//                                                });
//
//
                                              _mySelection = newValue;
                                              _mySelectionString =
                                                  getSelectedValue(
                                                      int.parse(_mySelection));
                                              showDialog(
                                                context: context,
                                                barrierDismissible: false,
                                                child: new AlertDialog(
                                                  title: new Text(
                                                    "Staff Action",
                                                    style: TextStyle(
                                                        color: Colors.red,
                                                        fontSize: 15.0,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily: 'serif'),
                                                  ),
                                                  content: Text(
                                                      "Your Edit Action is \" $_mySelectionString\" for ${data[1]} of ${data[2]} Department."),
                                                  actions: <Widget>[
                                                    ButtonBar(
                                                      children: <Widget>[
                                                        new FlatButton(
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context,
                                                                    rootNavigator:
                                                                        true)
                                                                .pop('dialog');
                                                            _mySelection = null;
                                                            _mySelectionString =
                                                                null;
                                                          },
                                                          child: new Text(
                                                            'No',
                                                            style: TextStyle(
                                                                color:
                                                                    Colors.red,
                                                                fontSize: 15.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontFamily:
                                                                    'serif'),
                                                          ),
                                                        ),
                                                        FlatButton(
                                                          child: Text(
                                                            'Submit',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .green,
                                                                fontSize: 15.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontFamily:
                                                                    'serif'),
                                                          ),
                                                          onPressed: () {
                                                            performEditOperation(
                                                                context);
                                                            Navigator.of(
                                                                    context,
                                                                    rootNavigator:
                                                                        true)
                                                                .pop('dialog');
                                                          },
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              );
//
                                            },
                                            items: leaveDutyType.map((Map map) {
                                              return new DropdownMenuItem<
                                                  String>(
                                                value: map["id"].toString(),
                                                child: new Text(
                                                  map["name"],
                                                ),
                                              );
                                            }).toList(),
                                          ),
                                          actions: <Widget>[
                                            ButtonBar(
                                              children: <Widget>[
                                                FlatButton(
                                                  child: Text(
                                                    'Exit',
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 15.0,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily: 'serif'),
                                                  ),
                                                  onPressed: () {
                                                    Navigator.of(context,
                                                            rootNavigator: true)
                                                        .pop('dialog');
                                                    _mySelection = null;
                                                    _mySelectionString = null;
                                                  },
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      );
//                                        Alert(
//                                            context: context,
//                                            title: "Staff Action",
//                                            desc:
//                                                "Mark present, leave, duty for : ${data[1]}\n\n",
//                                            content: DropdownButton<String>(
//                                              isDense: true,
//                                              hint:
//                                                  new Text("<<Select Option>>"),
//                                              value: _mySelection,
//                                              onChanged: (String newValue) {
//                                                Navigator.of(context,
//                                                        rootNavigator: true)
//                                                    .pop('dialog');
//                                              },
//                                              items:
//                                                  leaveDutyType.map((Map map) {
//                                                return new DropdownMenuItem<
//                                                    String>(
//                                                  value: map["id"].toString(),
//                                                  child: new Text(
//                                                    map["name"],
//                                                  ),
//                                                );
//                                              }).toList(),
//                                            ),
//                                            closeFunction: () {
//                                              Navigator.of(context).pop();
//                                            }).show();
                                    },
                                    color: Colors.red,
                                  ),
                                ],
                              ),
                            ],
                          ));
                    },
                    child: new Container(
                        child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Icon(Icons.person),
                        new SizedBox(
                          width: _width / 30,
                        ),
                        new Text('EDIT')
                      ],
                    )),
                    color: Colors.blue[50],
                  ),
                ),
              ],
            ),
          )),
        )
      ],
    );
  }

  Future<String> performEditOperation(context) async {
    sharedPreferences = await SharedPreferences.getInstance();
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String today = formatter.format(now);
    print("DATA : " + _mySelection.toString());
    try {
      var bdata = {
        "duration": 1,
        "endDate": today,
        "facultyIds": [data[0]],
        "instituteId":
            sharedPreferences.getInt(CommonSharedPreferance.INSTITUTE_ID),
        "reasonByPrincipal": _mySelectionString,
        "startDate": today,
        "typeId": int.parse(_mySelection)
      };
      var response = await http.post(
        Uri.parse(CommonConstants.HOST_URL + CommonConstants.FAC_ATT_BYPRI_API),
        body: json.encode(bdata),
        headers: CommonConstants.HOST_HEADER,
      );

      var datas = jsonDecode(response.body);
      print("Successfully : " + datas.toString());

      //print("Datass : " + leaveDutyType.toString());
      _mySelection = null;
      _mySelectionString = null;
    } on Exception catch (_) {
      print('never reached');
    } finally {
      var route = new MaterialPageRoute(
        builder: (BuildContext context) => new HomeEditView(),
      );
      Navigator.of(context).pop();
      Navigator.of(context).push(route);
    }
    return "Success";
  }

//  Future<bool> _onWillPop() async {
//    showDialog(
//      context: context,
//      builder: (context) => new AlertDialog(
//        title: new Text('Are you sure?'),
//        content: new Text('Do you want to edit attendance'),
//        actions: <Widget>[
//          new FlatButton(
//            onPressed: () => Navigator.of(context).pop(false),
//            child: new Text('No'),
//          ),
//          new FlatButton(
//            onPressed: () => performEditOperation(),
//            child: new Text('Yes'),
//          ),
//        ],
//      ),
//    );
//  }

  Widget rowCell(String count, String type) => new Expanded(
          child: new Column(
        children: <Widget>[
          new Text(
            '$count',
            style: new TextStyle(color: Colors.white),
          ),
          new Text(type,
              style: new TextStyle(
                  color: Colors.white, fontWeight: FontWeight.normal))
        ],
      ));
}
