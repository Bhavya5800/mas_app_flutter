import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:f_app/utils/flutkart.dart';
import 'package:f_app/utils/my_navigator.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    // Timer(Duration(seconds: 5), () => MyNavigator.goToLogin(context));
  }

  AnimationController animCtrl;
  Animation<double> animation;
  SharedPreferences sharedPreferences;
  AnimationController animCtrl2;
  Animation<double> animation2;

  bool showFirst = true;
  int count = 1;
  Future<String> versionVerify( BuildContext context) async {
    if(count==1) {
      int version = 99;
      int uId = 0;

      sharedPreferences = await SharedPreferences.getInstance();
      //uId = sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID);

      if (uId != 0 && uId != null) {
        var response = await http.get(
          Uri.parse(CommonConstants.HOST_URL +
              CommonConstants.FAC_TODAY_ATT_STS +
              uId.toString() +
              "/" +
              version.toString()),
        );
        //  print(response.body);
        String message = "";
        if (response.body != null) {
          var resData = json.decode(response.body);
          if (resData["message"] == "Lower Version!") {
            message = "OLDER Version is Detected";
          } else if (resData["message"] == "Invalid User Role...") {
            message = "User Role is invalided!";
          } else if (resData["message"] == "User account deactivated...") {
            message = "User Account is deactivated !";
          } else if (resData["message"] == "Successfully") {
            message = "Succesfully Login";
            sharedPreferences.setInt(
                CommonSharedPreferance.FA_ID, resData["faId"]);
            sharedPreferences.setString(
                CommonSharedPreferance.IN_TIME, resData["inTime"]);
            sharedPreferences.setString(
                CommonSharedPreferance.OUT_TIME, resData["outTime"]);
            //OTHER Paramter for access key property.
            sharedPreferences.setBool(
                CommonSharedPreferance.IS_ACTIVE, resData["isActive"]);
            Timer(Duration(seconds: 3), () => MyNavigator.goToLogin(context));
            //sharedPreferences.setBool(CommonSharedPreferance, resData["isMemo"]);
          } else {
            message = "Invalid Details";
          }
        } else {
          message = "Invalid Connection";
        }
        log("SP: $message");
        print(message);
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text(message)));
      }
      else {
        log("SP: SP Not Set yet!");
        Timer(Duration(seconds: 5), () => MyNavigator.goToLogin(context));
      }
    }
    count+=1;
    // Do coding here after getting result from REST.
    return "success";
  }

//  @override
//  Future<void> initState() async {
//    super.initState();
//    sharedPreferences = await SharedPreferences.getInstance();
//
//    // Animation init
//    animCtrl = new AnimationController(
//        duration: new Duration(milliseconds: 500), vsync: this);
//    animation = new CurvedAnimation(parent: animCtrl, curve: Curves.easeOut);
//    animation.addListener(() {
//      this.setState(() {});
//    });
//    animation.addStatusListener((AnimationStatus status) {});
//
//    animCtrl2 = new AnimationController(
//        duration: new Duration(milliseconds: 1000), vsync: this);
//    animation2 = new CurvedAnimation(parent: animCtrl2, curve: Curves.easeOut);
//    animation2.addListener(() {
//      this.setState(() {});
//    });
//    animation2.addStatusListener((AnimationStatus status) {});
//    // Check for Weather Internet is available or not their.
//    try {
//      final result = await InternetAddress.lookup('google.com');
//      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
//        print('connected');
//      }
//    } on SocketException catch (_) {
//      print('not connected');
//    }
//  }

  @override
  Widget build(BuildContext context) {
    versionVerify(context);
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: Colors.indigo),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 50.0,
                        child: StreamBuilder<String>(
                          builder: (context, snapshot) => Image(
                            height: 68.0,
                            width: 68.0,
                            semanticLabel: "Mobile Attendance System",
                            image: AssetImage("assets/image/proffes.png"),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Text(
                        Flutkart.name,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 24.0),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Text(
                      Flutkart.store,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                          color: Colors.white),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
