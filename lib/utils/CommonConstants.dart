class CommonConstants {
  static const String HOST_TYPE = "http://";

  //    static const String HOST_TYPE="https://";
  //     static const String HOST = "10.16.1.27:8089/springWebAPI";
  //    static const String HOST="192.168.1.3:8089/springWebAPI";
  static const String HOST = "www.attendance.cteguj.in/springWebAPI";
  static const String HOST_URL = HOST_TYPE + HOST;

  static const HOST_HEADER = {
    "accept": "application/json",
    "content-type": "application/json",
  };

  static const String PackageName = "in.cteguj.jobs.mobileattendancesystemapp";

  /// REST METHOD : GET
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String APP_VERSION_API = "/api/androidVersion/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_ACT_UPDATE_API = "/api/faculty/activity/update";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String STD_ATT_TOT_API = "/api/student/attendance/total";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_ATT_BYPRI_API = "/api/facultyAttendance/by/institute";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String LEAVETYPE_API = "/api/leaveTypeMaster/list";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String CHANGE_PWD_API = "/api/change/password";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String TIMETABLE_API = "/api/timetable/list";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_ACT_RECORD_API = "/api/faculty/activity/record";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_ATT_LIST_API = "/api/facultyAttendance/list";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String LEAVE_STATUS_DAY_API = "/api/trnDtlLeaveStatus/Day/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String INST_MEMO_API = "/api/trnDtlIssueMemo/institute/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String ATT_LATE_REASON_API =
      "/api/facultyAttendance/late/reason";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_PRO_EDIT_API = "/api/faculty/profile/edit";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_DATA_API = "/api/faculty/data/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String DEPT_BYPRI_API = "/api/department/by/institute/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_SHIFT_API = "/api/facultyShiftMpg";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String INST_SHIFT_API = "/api/shift/institute/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String MEMO_UPDATE_BYFAC_API =
      "/api/trnDtlIssueMemo/updatebyFaculty";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String MEMO_API = "/api/trnDtlIssueMemo/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_ATT_VERIFY_API = "/api/verify/attendance/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String STD_FEEDBACK_API = "/api/student/feedback";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String STD_TIMETABLE_API = "/api/student/timetable/info";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_LEC_INFO_API = "/api/faculty/lecture/info";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_TODAY_ATT_API = "/api/facultyAttendance/today/by/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String LEACE_STATUS_API = "/api/trnDtlLeaveStatus/today/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String PWD_RESET_API = "/api/resetPassword";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_ACT_LIST_API = "/api/faculty/activity/list";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String SLOT_API = "/api/slot/list/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String STD_ATT_DETAILS_API = "/api/student/attendance/detail/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String STD_ATT_DAILY_API = "/api/student/attendance/daily";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String STD_ATT_LIST_API = "/api/student/attendance/list";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_LIST_API = "/api/faculty/list/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_MEMO_API = "/api/trnDtlIssueMemo/faculty/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String INST_FAC_PR_LIST_API = "/api/faculty/list/present/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_LOGIN_API = "/api/faculty/login/";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String CHECK_IN_ATT_API = "/api/check/in/attendance";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 09-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String CHECK_OUT_ATT_API = "/api/check/out/attendance";

  /// REST METHOD :
  /// REST MODEL CLASS :
  /// REST ARGS :
  /// REST MODULE :
  /// ANDROID MODEL (IF) :
  /// CREATE DATE : 13-DEC-2019 @BHAVYA SHAH
  /// MODIFY DATE :
  /// MODIFY BY :
  /// DESCRIPTION :
  static const String FAC_TODAY_ATT_STS = "/api/faculty/today/status/";
}

class DisplayMSG {
  static const String NET_NOT_WORKING = "Not Connected to MAS Service!";
  static const String LOADING = "Data Processing...";
  static const String MAS_LOADING = "MAS Processing...";
  static const String PLAYSTORE_URL = "market://details?id=";

  static const String PLAYSTORE_URL2 =
      "https://play.google.com/store/apps/details?id=";
}
