class CommonSharedPreferance{

  static const String  INSTITUTE_NAME = "inst_nm";
  static const String  FACULTY_NAME = "fac_nm";
  static const String  DEPARTMENT_NAME = "dept_nm";
  static const String INSTITUTE_ID = "inst_id";
  static const String FACULTY_ID = "fac_id";
  static const String DEPARTMENT_ID = "dept_id";
  static const String INSTITUTE_TYPE_ID = "inst_type_id";
  static const String IN_TIME = "in_time";
  static const String OUT_TIME = "out_time";
  static const String ATT_DATE = "att_date";
  static const String FA_ID  = "fa_id";
  static const String ROLE = "role";
  static const String EMAIL = "email";
  static const String PASSWORD = "password";
  static const String DATE_IN_Time = "Date_IN_Time";
  static const String PREF_NAME = "FAS";

  // All Shared Preferences Keys
  static const String IS_LOGIN = "IsLoggedIn";
  static const String DATE_MNG = "Date_Mng";
  static const String IS_ACTIVE = "isActive";
  static const String KEY_TOKEN_T = "TOKEN_T";
  static const String KEY_TOKEN = "TOKEN";
  static const String DATE_IN_Time_S = "Date_IN_Time_S";
  static const String DATE_OUT_Time_S = "Date_OUT_Time_S";

}