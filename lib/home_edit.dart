import 'dart:convert';

import 'package:f_app/EditUserAttendance.dart';
import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'Trip.dart';

class HomeEditView extends StatefulWidget {
  //   final List<Trip> tripsList = [
  //    Trip("Bhavya shah", DateTime.now(), DateTime.now(), 200, "C.E"),
  //    Trip("Hemant Joshi", DateTime.now(), DateTime.now(), 450, "Civil"),
  //    Trip("Dhaval Sathvara", DateTime.now(), DateTime.now(), 900, "Admin"),
  //    Trip("Parth Modi", DateTime.now(), DateTime.now(), 170, "I.C"),
  //    Trip("Tushar Raval", DateTime.now(), DateTime.now(), 180, "C.E"),
  //  ];

  @override
  _HomeEditViewState createState() => _HomeEditViewState();
}

class _HomeEditViewState extends State<HomeEditView> {
  final List<Faculty> facultyList = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  List<Map> leaveDutyType = [];

  Future<List<Faculty>> getData() async {
    sharedPreferences = await SharedPreferences.getInstance();

    var response = await http.get(
      Uri.parse(CommonConstants.HOST_URL +
          CommonConstants.INST_FAC_PR_LIST_API +
          sharedPreferences
              .getInt(CommonSharedPreferance.INSTITUTE_ID)
              .toString()),
      headers: CommonConstants.HOST_HEADER,
    );

    var data = jsonDecode(response.body);
    final dlist = data as List;
    // final dlist = ddmap["content"] as List;
    print("Data : " + dlist.toString());

    int cnt = 1;
    for (var name in dlist) {
      final dmap = name as Map;
      int fId = dmap["id"];
      String fName = dmap["fName"];
      String departmentName = dmap["departmentName"];
      bool presentStatus = dmap["status"];
      int leaveStatus = dmap["leaveStatus"];
      String status = "Present";
      if (presentStatus) {
        if (leaveStatus == 2) {
          status = "By Pri.";
        } else if (leaveStatus == 1) {
          status = "Duty";
        } else if (leaveStatus == 0) {
          status = "Present";
        }
      } else {
        if (leaveStatus == 1) {
          status = "Leave";
        } else if (leaveStatus == 2) {
          status = "Weekend";
        } else if (leaveStatus == 3) {
          status = "Holiday";
        } else if (leaveStatus == 4) {
          status = "Vacation";
        } else {
          status = "other";
        }
      }
      facultyList
          .add(Faculty(fName, DateTime.now(), status, departmentName, fId));
      cnt += 1;
    }
    response = await http.get(
      Uri.parse(CommonConstants.HOST_URL + CommonConstants.LEAVETYPE_API),
      headers: CommonConstants.HOST_HEADER,
    );

    data = jsonDecode(response.body);
    final Typedlist = data as List;
    // print("Data : " + Typedlist.toString());

    cnt = 1;
    for (var names in Typedlist) {
      final dmap = names as Map;
      // final dlist = ddmap["content"] as List;
      //  print("Data : " + Typedlist.toString());
      int id = dmap["id"];
      String name = dmap["name"];
      Map mp = {"id": id, "name": name};
      leaveDutyType.add(mp);
    }
    print("Datasss : " + leaveDutyType.toString());
    return facultyList;
  }

  @override
  Widget build(BuildContext context) {
    //getData();
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white70,
      appBar: AppBar(
        title: Text("Mark Staff Attendance"),
      ),
      body: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            return snapshot.data != null
                ? buildTripCard(snapshot.data)
                : Center(child: CircularProgressIndicator());
          }),
    );
//      Container(
//
//      child: new ListView.builder(
//          itemCount: tripsList.length,
//          itemBuilder: (BuildContext context, int index) =>
//              buildTripCard(context, index)),
//      color: Colors.white70,
//      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
//    );
  }

  Widget buildTripCard(List<Faculty> faculty) {
    if (faculty.length == 0) {
      return new Container(
          child: Center(
        child: Text(
          "No Data Found..",
          style: new TextStyle(fontSize: 22.0),
        ),
      ));
    } else {
      return new Container(
        child: ListView.builder(
            itemCount: faculty.length,
            padding: const EdgeInsets.all(2.0),
            itemBuilder: (context, position) {
              return Card(
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 1.0, bottom: 1.0),
                        child: Row(children: <Widget>[
                          Text(
                            faculty[position].fname,
                            style: new TextStyle(fontSize: 22.0),
                          ),
                          Spacer(),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, bottom: 5.0),
                        child: Row(children: <Widget>[
                          Text(
                            faculty[position].dName,
                            style: new TextStyle(fontSize: 14.0),
                          ),
                          Spacer(),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, bottom: 5.0),
                        child: Row(children: <Widget>[
                          Text(
                            faculty[position].status,
                            style: new TextStyle(fontSize: 17.0),
                          ),
                          Spacer(),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 1.0, bottom: 1.0),
                        child: Row(
                          children: <Widget>[
                            Spacer(),
                            ButtonBar(
                              children: <Widget>[
                                FlatButton(
                                  child: Text(
                                    'Edit',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'serif'),
                                  ),
                                  onPressed: () {
//                                    Alert(
//                                            context: context,
//                                            title: "Edit Staff Action",
//                                            desc:
//                                                "By Principal Marked attendance updated to present, leave, duty for : " +
//                                                    faculty[position]
//                                                        .fId
//                                                        .toString())
//                                        .show();
                                    List data = [
                                      faculty[position].fId,
                                      faculty[position].fname,
                                      faculty[position].dName,
                                      faculty[position].status
                                    ];
                                    show(context, data);
                                  },
                                  color: Colors.indigo,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
      );
    }
  }

  show(BuildContext context, List data) {
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new EditUserAttendance(data),
    );
    Navigator.of(context).pop();
    Navigator.of(context).push(route);
  }
}
