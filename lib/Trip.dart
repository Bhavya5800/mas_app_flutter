class Trip {
  final String title;
  final DateTime startDate;
  final DateTime endDate;
  final int budget;
  final String travelType;
  String dName;

  Trip(this.title, this.startDate, this.endDate, this.budget, this.travelType);
}

class Faculty {
  final String fname;
  final DateTime attDate;
  final String status;
  final String dName;
  final int fId;

  Faculty(this.fname, this.attDate, this.status, this.dName, this.fId);
}

class DailyTask {
  String slotNo;
  String title;
  String departmentName;
  String subjectName;
  String div;
  int tCount;
  int pCount;
  int slotId;

  DailyTask(this.slotNo, this.title, this.departmentName, this.subjectName,
      this.div, this.tCount, this.pCount, this.slotId);
}

class Notice {
  final String staffName;
  final String departmentName;
  final String staffReply;
  final String noticeDate;
  final String noticeStatus;
  final int noticeNo;
  final String principalReply;
  final int memoId;
  final int fId;

  Notice(
      this.staffName,
      this.departmentName,
      this.staffReply,
      this.noticeDate,
      this.noticeStatus,
      this.noticeNo,
      this.principalReply,
      this.memoId,
      this.fId);
}

class FacultyNotice {
  final String noticeDate;
  final bool noticeStatus;
  final int noticeNo;
  final int memoId;

  FacultyNotice(this.noticeDate, this.noticeStatus, this.noticeNo, this.memoId);
}

class LecLabDetailsClass {
  String batchName;
  int dayId;
  int facultyId;
  String lecOrLab;
  int semId;
  int slotId;
  String subjectName;
  int totCount;
  int totPresent;
  String departmentName;
  String divison;

//  {
//  "batchName": "string",
//  "dayId": 0,
//  "facultyId": 0,
//  "lecOrLab": "string",
//  "semId": 0,
//  "slotId": 0,
//  "subjectName": "string",
//  "totCount": 0,
//  "totPresent": 0
//  }

//STD_ATT_TOT_API

}

class FacultyDetails {
  int fId;
  String fName;
  String mName;
  String lName;
  String phone;
  String email;
  String salutationName;
  String desgName;
  String deptName;
  String className;
  int classNo;
  int deptId;
  int desgId;
}
