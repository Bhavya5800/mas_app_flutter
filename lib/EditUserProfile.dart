import 'dart:convert';

import 'package:f_app/Trip.dart';
import 'package:f_app/UserProfile.dart';
import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

final key = new GlobalKey<ScaffoldState>();

class EdituserProfile extends StatefulWidget {
  final userData;
  final deptData;

  EdituserProfile(this.userData, this.deptData);
  static Map<String, int> classDesignationMap = {
    "PRINCIPAL": 1,
    "PROF.(CL-1)": 2,
    "HOD": 3,
    "ASSO.PROF. (CL-1)": 4,
    "AP (Cl-2)": 5,
    "LECTURER": 6,
    "AO(DTE)": 8,
    "PROGRAMMER": 52,
    "OS(DTE)": 7,
    "ACCOUNT OFFICER": 9,
    "ASST. LAIBRARIAN": 10,
    "ASST. LECTURER": 11,
    "CARPENTER": 12,
    "CATALOGER": 13,
    "COMP.OPERATOR": 15,
    "CURETOR": 17,
    "DATA ENTRY OPERATOR": 18,
    "DRAFTSMAN": 19,
    "ELECTRICIAN": 21,
    "FITTER": 22,
    "FOREMEN": 23,
    "HEAD CLERK": 25,
    "INSTRUCTOR GRADE-B": 26,
    "INSTRUCTOR GRADE-C": 27,
    "JR. CLERK CUM TYPIST": 28,
    "JUNIOR CLERK": 29,
    "JUNIOR LECTURER": 30,
    "LAB.ASSITANT": 31,
    "LIBRARIAN": 32,
    "LIBRARY CLERK": 33,
    "MACHINIST": 34,
    "MASCHINE ATTENDANT": 36,
    "MECHENIC": 37,
    "OFFICE SUPRITENDENT": 38,
    "SENIOR CLERK": 43,
    "SHOP LAB ATTENDANT": 44,
    "STORE CLERK": 45,
    "STORE KEEPER": 46,
    "TURNER": 48,
    "WELDER": 50,
    "WORKSHOP ATTENDANT": 51,
    "CHOKIDAR": 14,
    "COUNTER ATTENDENT": 16,
    "DRIVER": 20,
    "HAMAL": 24,
    "MALI": 35,
    "PART-TIME HAMAL": 39,
    "PEON": 40,
    "PEON CUM DRIVER": 41,
    "PUMP  OPERATOR": 42,
    "SWEEPER": 47,
    "WATCHMAN": 49,
  };

  static Map<int, String> classDesignationGetMap = {
    1: "PRINCIPAL",
    2: "PROF.(CL-1)",
    3: "HOD",
    4: "ASSO.PROF. (CL-1)",
    5: "AP (Cl-2)",
    6: "LECTURER",
    8: "AO(DTE)",
    52: "PROGRAMMER",
    7: "OS(DTE)",
    9: "ACCOUNT OFFICER",
    10: "ASST. LAIBRARIAN",
    11: "ASST. LECTURER",
    12: "CARPENTER",
    13: "CATALOGER",
    15: "COMP.OPERATOR",
    17: "CURETOR",
    18: "DATA ENTRY OPERATOR",
    19: "DRAFTSMAN",
    21: "ELECTRICIAN",
    22: "FITTER",
    23: "FOREMEN",
    25: "HEAD CLERK",
    26: "INSTRUCTOR GRADE-B",
    27: "INSTRUCTOR GRADE-C",
    28: "JR. CLERK CUM TYPIST",
    29: "JUNIOR CLERK",
    30: "JUNIOR LECTURER",
    31: "LAB.ASSITANT",
    32: "LIBRARIAN",
    33: "LIBRARY CLERK",
    34: "MACHINIST",
    36: "MASCHINE ATTENDANT",
    37: "MECHENIC",
    38: "OFFICE SUPRITENDENT",
    43: "SENIOR CLERK",
    44: "SHOP LAB ATTENDANT",
    45: "STORE CLERK",
    46: "STORE KEEPER",
    48: "TURNER",
    50: "WELDER",
    51: "WORKSHOP ATTENDANT",
    14: "CHOKIDAR",
    16: "COUNTER ATTENDENT",
    20: "DRIVER",
    24: "HAMAL",
    35: "MALI",
    39: "PART-TIME HAMAL",
    40: "PEON",
    41: "PEON CUM DRIVER",
    42: "PUMP  OPERATOR",
    47: "SWEEPER",
    49: "WATCHMAN",
  };
  @override
  _MyLEdituserProfile createState() =>
      new _MyLEdituserProfile(this.userData, this.deptData);
}

class _MyLEdituserProfile extends State<EdituserProfile> {
  var userData;
  final deptData;

  _MyLEdituserProfile(this.userData, this.deptData);

  SharedPreferences sharedPreferences;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  Map<String, int> classNoMap = {
    'CLASS-1': 1,
    'CLASS-2': 2,
    'CLASS-3': 3,
    'CLASS-4': 4,
  };
  Map<String, int> departmentMap = {};
  List<String> _salutations = <String>[
    '<SELECT ANY>',
    'Mr',
    'Ms',
    'Mrs',
    'Dr',
    'Prof'
  ];
  List<String> _classNos = <String>[
    '<SELECT ANY>',
    'CLASS-1',
    'CLASS-2',
    'CLASS-3',
    'CLASS-4',
  ];
  List<String> _classDescriptions = <String>[
    '<SELECT ANY>',
    "PRINCIPAL",
    "PROF.(CL-1)",
    "HOD",
    "ASSO.PROF. (CL-1)",
    "AP (Cl-2)",
    "LECTURER",
    "AO(DTE)",
    "PROGRAMMER",
    "OS(DTE)",
    "ACCOUNT OFFICER",
    "ASST. LAIBRARIAN",
    "ASST. LECTURER",
    "CARPENTER",
    "CATALOGER",
    "COMP.OPERATOR",
    "CURETOR",
    "DATA ENTRY OPERATOR",
    "DRAFTSMAN",
    "ELECTRICIAN",
    "FITTER",
    "FOREMEN",
    "HEAD CLERK",
    "INSTRUCTOR GRADE-B",
    "INSTRUCTOR GRADE-C",
    "JR. CLERK CUM TYPIST",
    "JUNIOR CLERK",
    "JUNIOR LECTURER",
    "LAB.ASSITANT",
    "LIBRARIAN",
    "LIBRARY CLERK",
    "MACHINIST",
    "MASCHINE ATTENDANT",
    "MECHENIC",
    "OFFICE SUPRITENDENT",
    "SENIOR CLERK",
    "SHOP LAB ATTENDANT",
    "STORE CLERK",
    "STORE KEEPER",
    "TURNER",
    "WELDER",
    "WORKSHOP ATTENDANT",
    "CHOKIDAR",
    "COUNTER ATTENDENT",
    "DRIVER",
    "HAMAL",
    "MALI",
    "PART-TIME HAMAL",
    "PEON",
    "PEON CUM DRIVER",
    "PUMP  OPERATOR",
    "SWEEPER",
    "WATCHMAN",
  ];
  List<String> _departments = <String>['<SELECT ANY>'];
  String _salutation = '<SELECT ANY>';
  String _classNo = '<SELECT ANY>';
  String _classDescription = '<SELECT ANY>';
  String _department = '<SELECT ANY>';
  LecLabDetailsClass labDetailsClass = new LecLabDetailsClass();

  setDepartmentData() {
//    facultyDetails.fName = userData['fname'];
//    facultyDetails.mName = userData['mname'];
//    facultyDetails.lName = userData['lname'];
//    facultyDetails.email = userData['email'];
//    facultyDetails.phone = userData['phone'];
//    facultyDetails.deptId = userData['departmentId'];
//    facultyDetails.desgId = userData['departmentId'];
//    facultyDetails.salutationName = userData['salutationName'];
//    facultyDetails.deptName = userData['departmentName'];
//    facultyDetails.desgName = userData['designationName'];

    _departments = [];
    _departments.add('<SELECT ANY>');

    for (var dName in deptData as List) {
      Map dMap = dName as Map;
      departmentMap.addAll({dMap["name"]: dMap["id"]});
      _departments.add(dMap["name"]);
    }
    _department = userData["departmentName"] != null
        ? userData["departmentName"]
        : '<SELECT ANY>';
    _salutation = userData["salutationName"] != null
        ? userData["salutationName"]
        : '<SELECT ANY>';
    _classDescription = userData["designationName"] != null
        ? userData["designationName"]
        : '<SELECT ANY>';
    _classNo = userData["desgClass"] != null
        ? 'CLASS-' + userData["desgClass"].toString()
        : '<SELECT ANY>';

    print("Department : " + _department);
    print("Designation : " + _classDescription);
    print("Class No : " + _classNo);
    print("Salutation : " + _salutation);
    if (_classNo == '<SELECT ANY>') {
      _classDescriptions = <String>['<SELECT ANY>'];
    } else if (_classNo == 'CLASS-1') {
      _classDescriptions = <String>[
        '<SELECT ANY>',
        "PRINCIPAL",
        "PROF.(CL-1)",
        "HOD",
        "ASSO.PROF. (CL-1)",
      ];
    } else if (_classNo == 'CLASS-2') {
      _classDescriptions = <String>[
        '<SELECT ANY>',
        "AP (Cl-2)",
        "LECTURER",
        "AO(DTE)",
        "PROGRAMMER",
      ];
    } else if (_classNo == 'CLASS-3') {
      _classDescriptions = <String>[
        '<SELECT ANY>',
        "OS(DTE)",
        "ACCOUNT OFFICER",
        "ASST. LAIBRARIAN",
        "ASST. LECTURER",
        "CARPENTER",
        "CATALOGER",
        "COMP.OPERATOR",
        "CURETOR",
        "DATA ENTRY OPERATOR",
        "DRAFTSMAN",
        "ELECTRICIAN",
        "FITTER",
        "FOREMEN",
        "HEAD CLERK",
        "INSTRUCTOR GRADE-B",
        "INSTRUCTOR GRADE-C",
        "JR. CLERK CUM TYPIST",
        "JUNIOR CLERK",
        "JUNIOR LECTURER",
        "LAB.ASSITANT",
        "LIBRARIAN",
        "LIBRARY CLERK",
        "MACHINIST",
        "MASCHINE ATTENDANT",
        "MECHENIC",
        "OFFICE SUPRITENDENT",
        "SENIOR CLERK",
        "SHOP LAB ATTENDANT",
        "STORE CLERK",
        "STORE KEEPER",
        "TURNER",
        "WELDER",
        "WORKSHOP ATTENDANT",
      ];
    } else if (_classNo == 'CLASS-4') {
      _classDescriptions = <String>[
        '<SELECT ANY>',
        "CHOKIDAR",
        "COUNTER ATTENDENT",
        "DRIVER",
        "HAMAL",
        "MALI",
        "PART-TIME HAMAL",
        "PEON",
        "PEON CUM DRIVER",
        "PUMP  OPERATOR",
        "SWEEPER",
        "WATCHMAN",
      ];
    }

    if (!_classDescriptions.contains(_classDescription)) {
      _classDescription = '<SELECT ANY>';
    }
  }

  FacultyDetails facultyDetails = new FacultyDetails();

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;
    setDepartmentData();
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      key: key,
      appBar: new AppBar(
        title: new Text("Edit Profile"),
      ),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
              key: _formKey,
              autovalidate: true,
              child: new ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                children: <Widget>[
                  new FormField(
                    builder: (FormFieldState state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.add_box),
                          labelText: 'Salutation',
                        ),
                        isEmpty: _salutation == _salutation,
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            value: _salutation,
                            isDense: true,
                            isExpanded: true,
                            onChanged: (String newValue) {
                              setState(() {
                                userData["salutationName"] = newValue;

                                _salutation = newValue;
                                state.didChange(newValue);
                              });
                            },
                            items: _salutations.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                    validator: (val) {
                      return val != '<SELECT ANY>'
                          ? null
                          : 'Please select a salutation';
                    },
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.perm_identity),
                      hintText: 'Enter First Name',
                      labelText: 'First Name',
                    ),
                    inputFormatters: [new LengthLimitingTextInputFormatter(30)],
                    validator: (val) =>
                        val.isEmpty ? 'First Name is required' : null,
                    onSaved: (val) => userData["fname"] = val,
                    initialValue: (userData["fname"].toString() != null
                        ? userData["fname"].toString()
                        : ""),
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.perm_identity),
                      hintText: 'Enter Middle Name',
                      labelText: 'Middle Name',
                    ),
                    initialValue: (userData["mname"].toString() != null
                        ? userData["mname"].toString()
                        : ""),
                    inputFormatters: [new LengthLimitingTextInputFormatter(30)],
                    onSaved: (val) => userData["mname"] = val,
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.perm_identity),
                      hintText: 'Enter Last Name',
                      labelText: 'Last Name',
                    ),
                    inputFormatters: [new LengthLimitingTextInputFormatter(30)],
                    initialValue: (userData["lname"].toString() != null
                        ? userData["lname"].toString()
                        : ""),
                    validator: (val) =>
                        val.isEmpty ? 'Last Name is required' : null,
                    onSaved: (val) => userData["lname"] = val,
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.phone),
                      hintText: 'Enter Mobile Number',
                      labelText: 'Mobile Number',
                    ),
                    initialValue: userData["phone"].toString(),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
                    validator: (val) =>
                        val.isEmpty ? 'Moblie Number is required' : null,
                    onSaved: (val) => userData["phone"] = val,
                  ),
                  new FormField(
                    builder: (FormFieldState state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.subject),
                          labelText: 'Department',
                        ),
                        isEmpty: _department == '<SELECT ANY>',
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            value: _department,
                            isDense: true,
                            isExpanded: true,
                            onChanged: (String newValue) {
                              setState(() {
                                userData["departmentName"] = newValue;
//                                labDetailsClass.semId = int.parse(newValue);
                                _department = newValue;
                                state.didChange(newValue);
                              });
                            },
                            items: _departments.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                    validator: (val) {
                      return val != '<SELECT ANY>'
                          ? null
                          : 'Please select a department';
                    },
                  ),
                  new FormField(
                    builder: (FormFieldState state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.class_),
                          labelText: 'Class',
                        ),
                        isEmpty: _classNo == '<SELECT ANY>',
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            value: _classNo,
                            isExpanded: true,
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                int cNo = null;
                                if ('CLASS-1' == newValue) {
                                  cNo = 1;
                                } else if ('CLASS-2' == newValue) {
                                  cNo = 2;
                                } else if ('CLASS-3' == newValue) {
                                  cNo = 3;
                                } else if ('CLASS-4' == newValue) {
                                  cNo = 4;
                                }
                                userData["desgClass"] = cNo;
                                // labDetailsClass.semId = int.parse(newValue);
                                _classNo = newValue;
                                state.didChange(newValue);
                              });
                            },
                            items: _classNos.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                    validator: (val) {
                      return val != '<SELECT ANY>'
                          ? null
                          : 'Please select a Class';
                    },
                  ),
                  new FormField(
                    builder: (FormFieldState state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.work),
                          labelText: 'Designation',
                        ),
                        isEmpty: _classDescription == '<SELECT ANY>',
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            value: _classDescription,
                            isExpanded: true,
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                userData["designationName"] = newValue;
                                //   labDetailsClass.divison = newValue;
                                _classDescription = newValue;
                                state.didChange(newValue);
                              });
                            },
                            items: _classDescriptions.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                    validator: (val) {
                      return val != '<SELECT ANY>'
                          ? null
                          : 'Please select a designation';
                    },
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(
                        left: _width / 8, right: _width / 8, top: 25),
                    child: new FlatButton(
                      onPressed: () {
                        fillDailyTask(context);
                      },
                      child: new Container(
                          child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Icon(Icons.person),
                          new SizedBox(
                            width: _width / 30,
                          ),
                          new Text('EDIT')
                        ],
                      )),
                      color: Colors.blue[50],
                    ),
                  ),
                ],
              ))),
    );
  }

//  Future<bool> _onWillPop() async {
//    return (await showDialog(
//      context: context,
//      builder: (context) => new AlertDialog(
//        title: new Text('Are you sure?'),
//        content: new Text('Do you want to submit task'),
//        actions: <Widget>[
//          new FlatButton(
//            onPressed: () => Navigator.of(context).pop(false),
//            child: new Text('No'),
//          ),
//          new FlatButton(
//            onPressed: () => fillDailyTask(context),
//            child: new Text('Yes'),
//          ),
//        ],
//      ),
//    )) ??
//        false;
//  }

  Future<String> fillDailyTask(context) async {
    final FormState form = _formKey.currentState;
    sharedPreferences = await SharedPreferences.getInstance();

    form.save();

    facultyDetails.fName = (userData['fname'] != null ? userData['fname'] : "");
    facultyDetails.mName =
        (userData['mname'] != null ? userData['mname'] + " " : "");
    facultyDetails.lName = (userData['lname'] != null ? userData['lname'] : "");
    facultyDetails.email =
        (userData['email'] != null ? userData['email'] : null);
    facultyDetails.phone =
        (userData['phone'] != null ? userData['phone'] : null);
    facultyDetails.salutationName =
        (userData['salutationName'] != null ? userData['salutationName'] : "");
    facultyDetails.deptName = (userData['departmentName'] != null
        ? userData['departmentName']
        : null);
    facultyDetails.desgName = (userData['designationName'] != null
        ? userData['designationName']
        : null);
    facultyDetails.classNo =
        (userData['desgClass'] != null ? userData['desgClass'] : null);

    print("fName : " + facultyDetails.fName);
    print("mName : " + facultyDetails.mName);
    print("lName : " + facultyDetails.lName);
    print("phone : " + facultyDetails.phone);
    if (facultyDetails.phone != null) {
      if (facultyDetails.phone.length < 10) {
        key.currentState.showSnackBar(
            SnackBar(content: Text("Mobile Number must be 10 digit!")));
        return "Success";
      }
    }

    if (facultyDetails.desgName != null) {
      facultyDetails.desgId =
          EdituserProfile.classDesignationMap[facultyDetails.desgName];
    } else {
      facultyDetails.desgId = null;
    }

    print("Gatting info : " + facultyDetails.desgId.toString());
    if (facultyDetails.deptName != null) {
      facultyDetails.deptId = departmentMap[facultyDetails.deptName];
    } else {
      facultyDetails.deptId = null;
    }
    var datas = {
      //  jsonObject.accumulate("id", UID);
//  jsonObject.accumulate("fName", facName.getText().toString().trim().toUpperCase());
//  jsonObject.accumulate("mName", mname);
//  jsonObject.accumulate("lName", facLName.getText().toString().trim().toUpperCase());
//  jsonObject.accumulate("salutationName", salName);
//  jsonObject.accumulate("departmentId", deptId);
//  jsonObject.accumulate("designationId", desId);
//  jsonObject.accumulate("classId", classId);
//  jsonObject.accumulate("name", name);
//  jsonObject.accumulate("phone", facPhone.getText().toString().trim());

      "id": sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
      "fName": facultyDetails.fName,
      "mName": facultyDetails.mName,
      "lName": facultyDetails.lName,

      "salutationName": facultyDetails.salutationName,
      "departmentId": facultyDetails.deptId,
      "designationId": facultyDetails.desgId,
      "classId": facultyDetails.classNo,
      "name": facultyDetails.salutationName +
          " " +
          facultyDetails.fName +
          " " +
          facultyDetails.mName +
          facultyDetails.lName,
      "phone": facultyDetails.phone,
    };
    var response = await http.post(
      Uri.parse(CommonConstants.HOST_URL + CommonConstants.FAC_PRO_EDIT_API),
      body: json.encode(datas),
      headers: CommonConstants.HOST_HEADER,
    );

    var datass = jsonDecode(response.body);
    print(datass.toString());

    response = await http.get(
      Uri.parse(CommonConstants.HOST_URL +
          CommonConstants.FAC_DATA_API +
          sharedPreferences
              .getInt(CommonSharedPreferance.FACULTY_ID)
              .toString()),
      headers: CommonConstants.HOST_HEADER,
    );

    var datasss = jsonDecode(response.body);
    if (datasss["designationId"] != null) {
      datasss["designationName"] =
          EdituserProfile.classDesignationGetMap[datasss["designationId"]];
//      print("Actual Designation Name : "+datasss["designationName"]);

    }
    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new MyUserProlfile(datasss),
    );
    Navigator.of(context).pop();
    Navigator.of(context).pop();
    Navigator.of(context).push(route);
    return "Success";
  }

//  FAC_PRO_EDIT_API

//  void setDesignation() {
//    designation.put(1, "PRINCIPAL");
//    designation.put(2, "PROF.(CL-1)");
//    designation.put(3, "HOD");
//    designation.put(4, "ASSO.PROF. (CL-1)");
//    designation.put(5, "AP (Cl-2)");
//    designation.put(6, "LECTURER");
//    designation.put(8, "AO(DTE)");
//    designation.put(52, "PROGRAMMER");
//    designation.put(7, "OS(DTE)");
//    designation.put(9, "ACCOUNT OFFICER");
//    designation.put(10, "ASST. LAIBRARIAN");
//    designation.put(11, "ASST. LECTURER");
//    designation.put(12, "CARPENTER");
//    designation.put(13, "CATALOGER");
//    designation.put(15, "COMP.OPERATOR");
//    designation.put(17, "CURETOR");
//    designation.put(18, "DATA ENTRY OPERATOR");
//    designation.put(19, "DRAFTSMAN");
//    designation.put(21, "ELECTRICIAN");
//    designation.put(22, "FITTER");
//    designation.put(23, "FOREMEN");
//    designation.put(25, "HEAD CLERK");
//    designation.put(26, "INSTRUCTOR GRADE-B");
//    designation.put(27, "INSTRUCTOR GRADE-C");
//    designation.put(28, "JR. CLERK CUM TYPIST");
//    designation.put(29, "JUNIOR CLERK");
//    designation.put(30, "JUNIOR LECTURER");
//    designation.put(31, "LAB.ASSITANT");
//    designation.put(32, "LIBRARIAN");
//    designation.put(33, "LIBRARY CLERK");
//    designation.put(34, "MACHINIST");
//    designation.put(36, "MASCHINE ATTENDANT");
//    designation.put(37, "MECHENIC");
//    designation.put(38, "OFFICE SUPRITENDENT");
//    designation.put(43, "SENIOR CLERK");
//    designation.put(44, "SHOP LAB ATTENDANT");
//    designation.put(45, "STORE CLERK");
//    designation.put(46, "STORE KEEPER");
//    designation.put(48, "TURNER");
//    designation.put(50, "WELDER");
//    designation.put(51, "WORKSHOP ATTENDANT");
//    designation.put(14, "CHOKIDAR");
//    designation.put(16, "COUNTER ATTENDENT");
//    designation.put(20, "DRIVER");
//    designation.put(24, "HAMAL");
//    designation.put(35, "MALI");
//    designation.put(39, "PART-TIME HAMAL");
//    designation.put(40, "PEON");
//    designation.put(41, "PEON CUM DRIVER");
//    designation.put(42, "PUMP  OPERATOR");
//    designation.put(47, "SWEEPER");
//    designation.put(49, "WATCHMAN");
//
//  }
}
