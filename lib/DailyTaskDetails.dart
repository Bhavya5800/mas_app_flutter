import 'dart:convert';
import 'dart:ui' as ui;

import 'package:f_app/EditUserProfile.dart';
import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class DailyTaskViewDetails extends StatefulWidget {
  final data;
  DailyTaskViewDetails(this.data);
  @override
  _MyDailyTaskViewDetails createState() => new _MyDailyTaskViewDetails(data);
}

class _MyDailyTaskViewDetails extends State<DailyTaskViewDetails> {
  final data;
  _MyDailyTaskViewDetails(this.data);
  SharedPreferences sharedPreferences;

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;
    final String imgUrl =
        'https://rememberingplaces.com/wp-content/themes/listingpro/assets/images/admin/avtar.jpg';

    return new Stack(
      children: <Widget>[
        new Container(
          color: Colors.blue,
        ),
        new Image.network(
          imgUrl,
          fit: BoxFit.fill,
        ),
        new BackdropFilter(
            filter: new ui.ImageFilter.blur(
              sigmaX: 6.0,
              sigmaY: 6.0,
            ),
            child: new Container(
              decoration: BoxDecoration(
                color: Colors.blue.withOpacity(0.9),
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
              ),
            )),
        new Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: new AppBar(
            title: new Text("Daily Task Details"),
            centerTitle: false,
            elevation: 0.0,
            backgroundColor: Colors.transparent,
          ),
          backgroundColor: Colors.blue.withOpacity(0.9),
          body: SingleChildScrollView(
            child: new Center(
              child: new Column(
                children: <Widget>[
                  new SizedBox(
                    height: _height / 12,
                  ),
                  new CircleAvatar(
                    radius: _width < _height ? _width / 4 : _height / 4,
                    backgroundImage: NetworkImage(imgUrl),
                  ),
                  new SizedBox(
                    height: _height / 25.0,
                  ),
                  new Text(
                    'Department Name : ' + (data["batchName"]!= null ?data["batchName"].toString():"-"),
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    'Semester Id : ' + (data["semId"]!= null ?data["semId"].toString():"-") ,
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    'Subject Name : ' + (data["subjectName"]!= null ?data["subjectName"].toString():"-"),
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,

                        fontSize: 15,
                        color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    'Total Student : ' + (data["totalCount"]!= null ?data["totalCount"].toString():"-") ,
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    'Total Present : ' + (data["totPresent"]!= null ?data["totPresent"].toString():"-") ,
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    'Lecture Type : ' + (data["lecOrLab"]!= null ?data["lecOrLab"].toString():"-") ,

                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),

                ],
              ),
            ),
          ),
        )
      ],
    );
  }

//  Future<String> getData(context) async {
//    sharedPreferences = await SharedPreferences.getInstance();
//
//    var response = await http.get(
//      Uri.parse(CommonConstants.HOST_URL +
//          CommonConstants.DEPT_BYPRI_API +
//          sharedPreferences
//              .getInt(CommonSharedPreferance.INSTITUTE_ID)
//              .toString()),
//      headers: CommonConstants.HOST_HEADER,
//    );
//
//    var deptData = jsonDecode(response.body);
//    print("List Data : " + deptData.toString());
//    //  List<String> deptList = deptData as List;
//
//    //   print(deptData.toString());
//
//    var route = new MaterialPageRoute(
//      builder: (BuildContext context) => new EdituserProfile(data, deptData),
//    );
//    Navigator.of(context).push(route);
//
//    return "Success";
//  }

  Widget rowCell(String count, String type) => new Expanded(
      child: new Column(
        children: <Widget>[
          new Text(
            '$count',
            style: new TextStyle(color: Colors.white),
          ),
          new Text(type,
              style: new TextStyle(
                  color: Colors.white, fontWeight: FontWeight.normal))
        ],
      ));
}
