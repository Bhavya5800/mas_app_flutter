import 'dart:async';
import 'dart:convert';
import 'dart:convert' show jsonDecode;
import 'dart:developer';

import 'package:connectivity/connectivity.dart';
import 'package:device_info/device_info.dart';
import 'package:f_app/bloc.dart';
import 'package:f_app/dashboard.dart';
import 'package:f_app/pages/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ios_network_info/ios_network_info.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ForgetPasswordWebsiteElement extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Forget Password',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyForgetPasswordWebsiteElement(),
    );
  }
}

class MyForgetPasswordWebsiteElement extends StatefulWidget {
  @override
  SharedPreferences sharedPreferences;
  static var _pseudoController = new TextEditingController();
  static var _passwordController = new TextEditingController();
  static var _phoneController = new TextEditingController();
  static var data;

  static var _isSecured = true;

  //List data;
  String _bssid = 'Unknown BSSID';
  String _ssid = 'Unknown SSID';

  fetchAll() {
    fetchBssid();
    fetchSsid();
  }

  fetchBssid() async {
    try {
      _bssid = await IosNetworkInfo.bssid;
    } on Exception {
      _bssid = 'Failed to get WiFi BSSID.';
    }
  }

  fetchSsid() async {
    try {
      _ssid = await IosNetworkInfo.ssid;
    } on Exception {
      _ssid = 'Failed to get WiFi SSID.';
    }
  }

  Future<String> _getId(context) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme
        .of(context)
        .platform == TargetPlatform.iOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }


  Future<String> getData() async {
    var datas = {
      "email": "jyoti@test.com",
      "password": "12345",
      "androidId": "",
    };
    var response = await http.post(
      Uri.parse(
          "http://www.attendance.cteguj.in/springWebAPI/api/student/login/"),
      body: json.encode(datas),
      headers: {
        "accept": "application/json",
        "content-type": "application/json",
      },
    );

    if (response.body != null) {
      var resData = json.decode(response.body);
      if (resData["message"] == "Password updated successfully") {
        print(resData["message"]);
      }
      else {
        print(resData["message"]);
      }
    }
      return "fail";
    }

    /*Future<String> apiRequest(String url, Map jsonMap) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(jsonMap)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    return reply;
  }
  getData() async {
    String url =
        'http://www.dheemati.in/springWebAPI/api/student/login/';
    var datas = {
      'data':{
        "email": "jyoti@test.com",
        "password": "12345",
        "bssid": "72:bb:e9:b2:60:be"
      },
    };

    print(await apiRequest(url, datas));
  }*/

    String spEmail = "";
    String spPassword = "";
    String spPhone = "";

    checkSharedPrefernce() async {
      sharedPreferences = await SharedPreferences.getInstance();
      spEmail = sharedPreferences.getString("email");
      spPassword = sharedPreferences.getString("password");
      if (spEmail == null) {
        spEmail = " ";
      }
      if (spPassword == null) {
        spPassword = " ";
      }
      _pseudoController.text = spEmail;
      _passwordController.text = spPassword;
      _phoneController.text = spPassword;
      log("Data: load sharedPreferance");
    }
    int count = 1;
    @override
    Widget build(BuildContext context) {
      checkSharedPrefernce();
      final snackBar = SnackBar(
          content: Text('Invalid User Name and Password!'));
      final snackBar2 = SnackBar(content: Text('Connect Device With Wifi'));
      final bloc = Bloc();

      Future<String> getData(String nm, String pwd, context) async {
        sharedPreferences = await SharedPreferences.getInstance();

        var datas = {
          "email": nm,
          "password": pwd,
          "bssid": "34:02:9b:00:09:04",
          "versionCode": 99
        };
        var response = await http.post(
          Uri.parse(
              "http://www.attendance.cteguj.in/springWebAPI/api/faculty/login/"),
          body: json.encode(datas),
          headers: {
            "accept": "application/json",
            "content-type": "application/json",
          },
        );

        var connectivityResult = await (Connectivity().checkConnectivity());
        print(connectivityResult.toString());

        // I am connected to a wifi network.
        var wname = await (Connectivity().getWifiName());
        var wip = await (Connectivity().getWifiIP());
        print("Wifi IP : " + wip.toString());
        print("Wifi Name : " + wname.toString());

        var data = jsonDecode(response.body);

        if (data["faId"] > 0) {
          sharedPreferences.setString("email", nm);
          sharedPreferences.setString("password", pwd);
          log('data: Data Set in SP instance');

          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text(data["message"])));
          var route = new MaterialPageRoute(
            builder: (BuildContext context) => new PageTwo(),
          );
          Navigator.of(context).push(route);
        } else {
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text(data["message"])));
        }

        return "Success";
      }

      var emails = StreamBuilder<String>(
        stream: bloc.email,
        builder: (context, snapshot) =>
            TextFormField(
              onChanged: bloc.emailChanged,
              keyboardType: TextInputType.emailAddress,

              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "enter email",
                  labelText: "Email",
                  errorText: snapshot.error),
              controller: _pseudoController,
            ),
      );
      var phone = StreamBuilder<String>(
        builder: (context, snapshot) =>
            TextFormField(
              onChanged: bloc.emailChanged,
              keyboardType: TextInputType.emailAddress,

              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "enter phone",
                  labelText: "Email",
                  errorText: snapshot.error),
            ),
      );
      var passwords = StreamBuilder<String>(
        stream: bloc.password,
        builder: (context, snapshot) =>
            TextFormField(
              onChanged: bloc.passwordChanged,
              keyboardType: TextInputType.text,

              obscureText: true,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "enter password",
                  labelText: "Password",
                  errorText: snapshot.error),
              controller: _passwordController,
            ),
      );
      /*********************Alert Dialog Pseudo******************************/

      List<Widget> children = new List.generate(
          count, (int i) => new InputWidget(i));
      log("Data Msg : $count");
      return Scaffold(
        appBar: AppBar(
          title: Text("MAS Login"),
        ),
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery
                .of(context)
                .size
                .height,
            padding: EdgeInsets.all(16),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                StreamBuilder<String>(
                    builder: (context, snapshot) =>
                        Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              StreamBuilder<String>(
                                builder: (context, snapshot) =>
                                    Image(
                                      height: 100.0,
                                      width: 100.0,
                                      semanticLabel: "Mobile Attendance System",
                                      image: AssetImage(
                                          "assets/image/seals.png"),
                                    ),
                              ),
                              StreamBuilder<String>(
                                builder: (context, snapshot) =>
                                    Text(
                                      "Education Department\nGovernment of Gujarat\n(Mobile Attendance System)",
                                      style: TextStyle(
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'serif'),
                                    ),
                              ),
                            ])),
                StreamBuilder<String>(
                  builder: (context, snapshot) =>
                      Image(
                        height: 100.0,
                        width: 100.0,
                        semanticLabel: "Mobile Attendance System",
                        image: AssetImage("assets/image/proffes.png"),
                      ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                StreamBuilder<String>(
                  builder: (context, snapshot) =>
                      Text("Mobile Attendance System Forget Password"),
                ),
                SizedBox(
                  height: 20.0,
                ),
                emails,
                SizedBox(
                  height: 20.0,
                ), phone,
                SizedBox(
                  height: 20.0,
                ),
                passwords,
                SizedBox(
                  height: 20.0,
                ),
                SizedBox(
                  height: 20.0,
                ),
                StreamBuilder<bool>(
                  stream: bloc.submitCheck,
                  builder: (context, snapshot) =>
                      MaterialButton(
                        color: Colors.amber,
                        height: 50.0,
                        onPressed: () {
                          //Perform some action
                          fetchAll();
                          getData(
                              _pseudoController.text, _passwordController.text,
                              context);

                          /* if(result)
                         {
                           Scaffold.of(context).showSnackBar(snackBar);
                         }*/
                          //  print(result);
                        },
                        child: Text("Log-IN"),
                        minWidth: 400.0,
                      ),
                ),
                SizedBox(
                  height: 30.0,
                ),

                children.elementAt(0),
                SizedBox(
                  height: 20.0,
                ),
                StreamBuilder<String>(
                  builder: (context, snapshot) => Text("DTE Gujarat @ 2019"),
                ),
              ],
            ),
          ),
        ),
      );
    }

    @override
    State<StatefulWidget> createState() {
      // TODO: implement createState
      return null;
    }
  }

  class LoginPage extends StatefulWidget {
  final VoidCallback _onSignIn;

  LoginPage({@required onSignIn})
      : assert(onSignIn != null),
  _onSignIn = onSignIn;

  @override
  _LoginPageState createState() => _LoginPageState();
  }

  class _LoginPageState extends State<LoginPage> {
  // maintains validators and state of form fields
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();

  // manage state of modal progress HUD widget
  bool _isInAsyncCall = false;

  bool _isInvalidAsyncUser = false; // managed after response from server
  bool _isInvalidAsyncPass = false; // managed after response from server

  String _username;
  String spEmail = "TEXT";
  String _password;
  String spPassword = "TEXT";
  bool _isLoggedIn = false;

  // validate user name
  String _validateUserName(String userName) {
  if (userName.length < 8) {
  return 'Username must be at least 8 characters';
  }

  if (_isInvalidAsyncUser) {
  // disable message until after next async call
  _isInvalidAsyncUser = false;
  return 'Incorrect user name';
  }

  return null;
  }

  // validate password
  String _validatePassword(String password) {
  if (password.length < 8) {
  return 'Password must be at least 8 characters';
  }

  if (_isInvalidAsyncPass) {
  // disable message until after next async call
  _isInvalidAsyncPass = false;
  return 'Incorrect password';
  }

  return null;
  }
  CircularProgressIndicator circularProgressIndicator = CircularProgressIndicator();
  void _submit() {

  if (_loginFormKey.currentState.validate()) {
  _loginFormKey.currentState.save();

  // dismiss keyboard during async call
  FocusScope.of(context).requestFocus(new FocusNode());

  // start the modal progress HUD
  setState(() {
  _isInAsyncCall = true;
  });

  // Simulate a service call
  Future.delayed(Duration(seconds: 1), () {
  final _accountUsername = 'username1';
  final _accountPassword = 'password1';
  setState(() {
  if (_username == _accountUsername) {
  _isInvalidAsyncUser = false;
  if (_password == _accountPassword) {
  // username and password are correct
  _isInvalidAsyncPass = false;
  _isLoggedIn = true;
  } else
  // username is correct, but password is incorrect
  _isInvalidAsyncPass = true;
  } else {
  // incorrect username and have not checked password result
  _isInvalidAsyncUser = true;
  // no such user, so no need to trigger async password validator
  _isInvalidAsyncPass = false;
  }
  // stop the modal progress HUD
  _isInAsyncCall = false;
  });
  if (_isLoggedIn)
  // do something
  widget._onSignIn();
  });
  }
  }

  TextEditingController _controller, _controllerPassword;

  @override
  Widget build(BuildContext context) {
  return Scaffold(
  appBar: AppBar(
  title: Text('Modal Progress HUD Demo'),
  backgroundColor: Colors.blue,
  ),
  // display modal progress HUD (heads-up display, or indicator)
  // when in async call
  body: ModalProgressHUD(
  child: SingleChildScrollView(
  child: Container(
  padding: const EdgeInsets.all(16.0),
  child: buildLoginForm(context),
  ),
  ),
  inAsyncCall: _isInAsyncCall,
  // demo of some additional parameters
  opacity: 0.5,
  progressIndicator: CircularProgressIndicator(),
  ),
  );
  }

  Widget buildLoginForm(BuildContext context) {
  final TextTheme textTheme = Theme.of(context).textTheme;
  // run the validators on reload to process async results

  _loginFormKey.currentState?.validate();
  return Form(
  key: this._loginFormKey,
  child: Column(
  children: [
  Padding(
  padding: const EdgeInsets.all(8.0),
  child: TextFormField(
  key: Key('username'),
  controller: _controller,
  decoration: InputDecoration(
  hintText: 'enter email', labelText: 'Email :'),
  style: TextStyle(fontSize: 20.0, color: textTheme.button.color),
  validator: _validateUserName,
  onSaved: (value) => _username = value,
  ),
  ),
  Padding(
  padding: const EdgeInsets.all(8.0),
  child: TextFormField(
  key: Key('password'),
  obscureText: true,
  controller: _controllerPassword,
  decoration: InputDecoration(
  hintText: 'enter password', labelText: 'Password :'),
  style: TextStyle(fontSize: 20.0, color: textTheme.button.color),
  validator: _validatePassword,
  onSaved: (value) => _password = value,
  ),
  ),
  Padding(
  padding: const EdgeInsets.all(32.0),
  child: RaisedButton(
  onPressed: _submit,
  child: Text('Login'),
  ),
  ),
  Padding(
  padding: const EdgeInsets.all(8.0),
  child: _isLoggedIn
  ? Text(
  'Login successful!',
  key: Key('loggedIn'),
  style: TextStyle(fontSize: 20.0),
  )
      : Text(
  'Not logged in',
  key: Key('notLoggedIn'),
  style: TextStyle(fontSize: 20.0),
  ),
  ),
  ],
  ),
  );
  }

  }
  class InputWidget extends StatelessWidget {

  final int index;
  static int count = 0;
  InputWidget(this.index);

  @override
  Widget build(BuildContext context) {
  count+=1;
  log("Data Index : $count");
  if(count>1)
  return new CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),);
  else
  return new CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.transparent),);
  }
  }
