import 'dart:async';
import 'dart:convert';
import 'dart:convert' show jsonDecode;
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:device_info/device_info.dart';
import 'package:f_app/AttendanceSummary.dart';
import 'package:f_app/ChangePassword.dart';
import 'package:f_app/DailyTask.dart';
import 'package:f_app/PrincipalStaffNotice.dart';
import 'package:f_app/StaffNotice.dart';
import 'package:f_app/bloc.dart';
import 'package:f_app/home_edit.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:ios_network_info/ios_network_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'EditUserProfile.dart';
import 'UserProfile.dart';
import 'home_view.dart';
import 'utils/CommonConstants.dart';

class MyGlobals {
  GlobalKey _scaffoldKey;

  MyGlobals() {
    _scaffoldKey = GlobalKey();
  }

  GlobalKey get scaffoldKey => _scaffoldKey;
}

final key = new GlobalKey<ScaffoldState>();

class PageTwo extends StatefulWidget {
  PageTwo();

  //   final List<Trip> tripsList = [
  //    Trip("Bhavya shah", DateTime.now(), DateTime.now(), 200, "C.E"),
  //    Trip("Hemant Joshi", DateTime.now(), DateTime.now(), 450, "Civil"),
  //    Trip("Dhaval Sathvara", DateTime.now(), DateTime.now(), 900, "Admin"),
  //    Trip("Parth Modi", DateTime.now(), DateTime.now(), 170, "I.C"),
  //    Trip("Tushar Raval", DateTime.now(), DateTime.now(), 180, "C.E"),
  //  ];

  @override
  _MyPageTwo createState() => _MyPageTwo();
}

class _MyPageTwo extends State<PageTwo> {
  _MyPageTwo();

//  LocationData currentLocation;
//  getUserLocation() async {//call this async method from whereever you need
//
//    LocationData myLocation;
//    String error;
//    Location location = new Location();
//    try {
//      myLocation = await location.getLocation();
//    } on PlatformException catch (e) {
//      if (e.code == 'PERMISSION_DENIED') {
//        error = 'please grant permission';
//        print(error);
//      }
//      if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
//        error = 'permission denied- please enable it from app settings';
//        print(error);
//      }
//      myLocation = null;
//    }
//    currentLocation = myLocation;
//    final coordinates = new Coordinates(
//        myLocation.latitude, myLocation.longitude);
//    var addresses = await Geocoder.local.findAddressesFromCoordinates(
//        coordinates);
//    var first = addresses.first;
//    print(' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');
//    return first;
//  }

  SharedPreferences sharedPreferences;

  static var _pseudoController = new TextEditingController();
  static var _passwordController = new TextEditingController();

  // var data;

  var inA;
  var outA;

  String inTime = "NULL";
  String outTime = "NULL";
  String instName = "";
  String facName = "";

//  String _bssid = '34:02:9b:00:09:04';
//  String _ssid = 'iOS';

  String _bssid = 'Unknown BSSID';
  String _ssid = 'Unknown SSID';

  fetchAll() {
    fetchBssid();
    fetchSsid();
  }

  fetchBssid() async {
    try {
      _bssid = await IosNetworkInfo.bssid;
      _bssid = (_bssid == null ? '34:02:9b:00:09:04' : _bssid);
    } on Exception {
      _bssid = 'Unknown BSSID';
    }
  }

  fetchSsid() async {
    try {
      _ssid = await IosNetworkInfo.ssid;
      _ssid = (_ssid == null ? 'NAMO WiFi' : _ssid);
    } on Exception {
      _ssid = 'Unknown SSID';
    }
  }

  var gridv = new GridView.count(
    crossAxisCount: 4,
    children: new List<Widget>.generate(16, (index) {
      return new GridTile(
        child: new Card(
            color: Colors.blue.shade200,
            child: new Center(
              child: new Text('tile $index'),
            )),
      );
    }),
  );

  Future<String> checkSharedPrefernce() async {
    sharedPreferences = await SharedPreferences.getInstance();

    if (sharedPreferences.getString(CommonSharedPreferance.IN_TIME) != null) {
      inTime = sharedPreferences.getString(CommonSharedPreferance.IN_TIME);
    }
    if (sharedPreferences.getString(CommonSharedPreferance.OUT_TIME) != null) {
      outTime = sharedPreferences.getString(CommonSharedPreferance.OUT_TIME);
    }
    if (sharedPreferences.getString(CommonSharedPreferance.INSTITUTE_NAME) !=
        null) {
      instName =
          sharedPreferences.getString(CommonSharedPreferance.INSTITUTE_NAME);
    }
    if (sharedPreferences.getString(CommonSharedPreferance.FACULTY_NAME) !=
        null) {
      facName =
          sharedPreferences.getString(CommonSharedPreferance.FACULTY_NAME);
    }
    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Bloc();

    void lateInDialog(context) {
      Future<String> getLInAtt(context, String reason) async {
        var datas = {
          "facultyId":
              sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
          "lateComingReason": reason,
        };
        var response = await http.post(
          Uri.parse(
              CommonConstants.HOST_URL + CommonConstants.ATT_LATE_REASON_API),
          body: json.encode(datas),
          headers: CommonConstants.HOST_HEADER,
        );

        var datass = jsonDecode(response.body) as Map;
        print("Late IN : " + datass.toString());
        if (datass["inTime"] != null)
          sharedPreferences.setString(
              CommonSharedPreferance.OUT_TIME, datass["inTime"]);
        var route = new MaterialPageRoute(
          builder: (BuildContext context) => new PageTwo(),
        );
        Navigator.of(context).pop();
        Navigator.of(context).push(route);

        return "Success";
      }

      var reason = StreamBuilder<String>(
        stream: bloc.password,
        builder: (context, snapshot) => TextField(
          onChanged: bloc.passwordChanged,
          keyboardType: TextInputType.text,
          maxLines: 3,
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: "Enter Reason",
              labelText: "Reason",
              errorText: snapshot.error),
          controller: _pseudoController,
        ),
      );

      showDialog(
          context: key.currentContext,
          barrierDismissible: false,
          child: new AlertDialog(
            title: new Text(
              " LATE IN ATTENDANCE ",
              style: TextStyle(
                  color: Colors.red,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'serif'),
            ),
            content: reason,
            actions: <Widget>[
              ButtonBar(
                children: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      inTime = "NULL";
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                    },
                    child: new Text(
                      'No',
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'serif'),
                    ),
                  ),
                  FlatButton(
                    child: Text(
                      'Submit Reason',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'serif'),
                    ),
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                      getLInAtt(context, _pseudoController.text);
                    },
                    color: Colors.blue,
                  ),
                ],
              ),
            ],
          ));
    }

    Future<String> getData() async {
      sharedPreferences = await SharedPreferences.getInstance();

      var response = await http.get(
        Uri.parse(CommonConstants.HOST_URL +
            CommonConstants.FAC_DATA_API +
            sharedPreferences
                .getInt(CommonSharedPreferance.FACULTY_ID)
                .toString()),
        headers: CommonConstants.HOST_HEADER,
      );

      var connectivityResult = await (Connectivity().checkConnectivity());
      print(connectivityResult.toString());

      var datass = jsonDecode(response.body);
      if (datass["designationId"] != null) {
        datass["designationName"] =
            EdituserProfile.classDesignationGetMap[datass["designationId"]];
      }
      print(datass);
      var route = new MaterialPageRoute(
        builder: (BuildContext context) => new MyUserProlfile(datass),
      );
      Navigator.of(context).push(route);

      return "Success";
    }

    void earlyOutDialog(context) {
      Future<String> getEoutAtt(context, String reason) async {
        var datas = {
          "facultyId":
              sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
          "earlyOutReason": reason,
        };
        var response = await http.post(
          Uri.parse(
              CommonConstants.HOST_URL + CommonConstants.ATT_LATE_REASON_API),
          body: json.encode(datas),
          headers: CommonConstants.HOST_HEADER,
        );
        var datass = jsonDecode(response.body) as Map;
        print("Early Out : " + datass.toString());
        if (datass["outTime"] != null)
          sharedPreferences.setString(
              CommonSharedPreferance.OUT_TIME, datass["outTime"]);
        var route = new MaterialPageRoute(
          builder: (BuildContext context) => new PageTwo(),
        );
        Navigator.of(context).pop();
        Navigator.of(context).push(route);

        return "Success";
      }

      var reason = StreamBuilder<String>(
        stream: bloc.password,
        builder: (context, snapshot) => TextField(
          onChanged: bloc.passwordChanged,
          keyboardType: TextInputType.text,
          maxLines: 3,
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: "Enter Reason",
              labelText: "Reason",
              errorText: snapshot.error),
          controller: _passwordController,
        ),
      );
      showDialog(
          context: key.currentContext,
          barrierDismissible: false,
          child: new AlertDialog(
            title: new Text(
              " EARLY OUT ATTENDANCE ",
              style: TextStyle(
                  color: Colors.red,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'serif'),
            ),
            content: reason,
            actions: <Widget>[
              ButtonBar(
                children: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      outTime = "NULL";
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                    },
                    child: new Text(
                      'No',
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'serif'),
                    ),
                  ),
                  FlatButton(
                    child: Text(
                      'Submit Reason',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'serif'),
                    ),
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                      getEoutAtt(context, _passwordController.text);
                    },
                    color: Colors.red,
                  ),
                ],
              ),
            ],
          ));
    }

    void successInDialog(context) {
      Future<String> successIn(context) async {
        var route = new MaterialPageRoute(
          builder: (BuildContext context) => new PageTwo(),
        );
        Navigator.of(context).pop();
        Navigator.of(context).push(route);

        return "Success";
      }

      showDialog(
          context: key.currentContext,
          barrierDismissible: true,
          child: new AlertDialog(
            title: new Text(
              "MARK IN ATTENDANCE SUCCESSFULLY!",
              style: TextStyle(
                  color: Colors.green,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'serif'),
            ),
            actions: <Widget>[
              ButtonBar(
                children: <Widget>[
                  FlatButton(
                    child: Text(
                      'Thank You',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'serif'),
                    ),
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                      successIn(context);
                    },
                    color: Colors.red,
                  ),
                ],
              ),
            ],
          ));
    }

    void successOutDialog(context) {
      //  fetchAll();
      Future<String> successoutAtt(context) async {
        var route = new MaterialPageRoute(
          builder: (BuildContext context) => new PageTwo(),
        );
        Navigator.of(context).pop();
        Navigator.of(context).push(route);

        return "Success";
      }

      showDialog(
          context: key.currentContext,
          barrierDismissible: false,
          child: new AlertDialog(
            title: new Text(
              "Successfully OUT ATTENDANCE Marked!",
              style: TextStyle(
                  color: Colors.green,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'serif'),
            ),
            actions: <Widget>[
              ButtonBar(
                children: <Widget>[
                  FlatButton(
                    child: Text(
                      'Thank You',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'serif'),
                    ),
                    onPressed: () {
                      successoutAtt(context);
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                    },
                    color: Colors.red,
                  ),
                ],
              ),
            ],
          ));
    }

//    void summaryTabClick(context) {
//      fetchAll();
//
//      showDialog(
//          barrierDismissible: false,
//          context: myGlobals.scaffoldKey.currentContext,
//          child: new AlertDialog(
//              title: new Text(
//                "Summary Tab Clicked!",
//                style: TextStyle(
//                    color: Colors.green,
//                    fontSize: 15.0,
//                    fontWeight: FontWeight.bold,
//                    fontFamily: 'serif'),
//              ),
//              actions: <Widget>[
//                ButtonBar(
//                  children: <Widget>[
//                    FlatButton(
//                      child: Text(
//                        'Thank You',
//                        style: TextStyle(
//                            color: Colors.white,
//                            fontSize: 15.0,
//                            fontWeight: FontWeight.bold,
//                            fontFamily: 'serif'),
//                      ),
//                      onPressed: () {
//                        print("Click Button!");
//                        Navigator.of(context, rootNavigator: true)
//                            .pop('dialog');
//                        var route = new MaterialPageRoute(
//                          builder: (BuildContext context) => new HomeView(),
//                        );
//                        Navigator.of(context).pop();
//                        Navigator.of(context).push(route);
//                      },
//                      color: Colors.red,
//                    ),
//                  ],
//                )
//              ]));
//    }

    Future<bool> _onWillPop() async {
      return (await showDialog(
            context: context,
            builder: (context) => new AlertDialog(
              title: new Text('Are you sure?'),
              content: new Text('Do you want to exit an App'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: new Text('No'),
                ),
                new FlatButton(
                  onPressed: () => exit(0),
                  child: new Text('Yes'),
                ),
              ],
            ),
          )) ??
          false;
    }

    Future<String> getInAtt(context) async {
      try {
        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          print('connected');
        }
      } on Exception catch (_) {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text("No Internet Connection!")));
        print('not connected');
        return "not connected";
      }
      //   getUserLocation();
      var connectivityResult = await (Connectivity().checkConnectivity());

      _bssid = '34:02:9b:00:09:04';
      _ssid = 'NAMO WiFi';
      String myIP = await (Connectivity().getWifiIP());
      if (myIP == null) {
        myIP = "0.0.0.0";
      }
      if (inTime != "NULL") {
        key.currentState.showSnackBar(
            SnackBar(content: Text("IN Attendance Already Marked!")));
        return "Success";
      }
      int role = sharedPreferences.getInt(CommonSharedPreferance.ROLE) == null
          ? 1
          : sharedPreferences.getInt(CommonSharedPreferance.ROLE);
      if (connectivityResult != ConnectivityResult.wifi && role != 3) {
        key.currentState
            .showSnackBar(SnackBar(content: Text("Connect to WiFi Network!")));
        return "Success";
      }

      if (role != 3) {
        fetchAll();
        print("BSSID : " + _bssid);
      }
      String identifier = "";
      if (Platform.isAndroid) {
        var androidInfo = await DeviceInfoPlugin().androidInfo;
        identifier = androidInfo.androidId; //UUID for Android
      }

      if (Platform.isIOS) {
        var iosInfo = await DeviceInfoPlugin().iosInfo;
        identifier = iosInfo.identifierForVendor;
      }

      //  _bssid = await IosNetworkInfo.bssid;
      var datas = {
        "fId": sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
        "bssid": _bssid,
        "ssid": _ssid,
        "isVerified": true,
        "versionCode": 99,
        "androidID": (identifier != null ? identifier : ""),
        "IMEINo": myIP,
        "faId": sharedPreferences.getInt(CommonSharedPreferance.FA_ID)
      };
      var response = await http.post(
        Uri.parse(CommonConstants.HOST_URL + CommonConstants.CHECK_IN_ATT_API),
        body: json.encode(datas),
        headers: CommonConstants.HOST_HEADER,
      );

      // I am connected to a wifi network.
//      var wname = await (Connectivity().getWifiName());
//      var wip = await (Connectivity().getWifiIP());
//      var _bssids = await (Connectivity().getWifiBSSID());
//      print("Wifi IP : " + wip.toString());
//      print("Wifi Name : " + wname.toString());

      // print(await Future.value(Connectivity().getWifiBSSID()));

      var datass = jsonDecode(response.body);
      if (datass["inTime"] != null) {
        inTime = datass["inTime"];
        sharedPreferences.setString(
            CommonSharedPreferance.IN_TIME, datass["inTime"]);

        String inFixTime = datass["instituteInTime"];
        String outFixTime = datass["instituteOutTime"];
        bool isLate = datass["isLate"];
        //    data["inTime"] = inTime;

        if (isLate) {
          lateInDialog(context);
        } else {
          successInDialog(context);
        }
      } else {
        key.currentState
            .showSnackBar(SnackBar(content: Text(datass["message"])));
        inTime = "NULL";
      }
      return "Success";
    }

    Future<String> getOutAtt(context) async {
      try {
        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          print('connected');
        }
      } on Exception catch (_) {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text("No Internet Connection!")));
        print('not connected');
        return "not connected";
      }
      _bssid = '34:02:9b:00:09:04';
      _ssid = 'NAMO WiFi';
      var connectivityResult = await (Connectivity().checkConnectivity());
      String myIP = await (Connectivity().getWifiIP());
      if (myIP == null) {
        myIP = "0.0.0.0";
      }
      if (inTime == "NULL") {
        key.currentState.showSnackBar(
            SnackBar(content: Text("IN Attendance Marke First!")));
        return "Success";
      } else if (outTime != "NULL") {
        key.currentState.showSnackBar(
            SnackBar(content: Text("OUT Attendance already Marked")));
        return "Success";
      }
      int role = sharedPreferences.getInt(CommonSharedPreferance.ROLE) == null
          ? 1
          : sharedPreferences.getInt(CommonSharedPreferance.ROLE);

      if (connectivityResult != ConnectivityResult.wifi && role != 3) {
        key.currentState
            .showSnackBar(SnackBar(content: Text("Connect to WiFi Network!")));
        return "Success";
      }
      String identifier = "";
      if (Platform.isAndroid) {
        var androidInfo = await DeviceInfoPlugin().androidInfo;
        identifier = androidInfo.androidId; //UUID for Android
      }

      if (Platform.isIOS) {
        var iosInfo = await DeviceInfoPlugin().iosInfo;
        identifier = iosInfo.identifierForVendor;
      }

      var datas = {
        "fId": sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
        "bssid": _bssid,
        "ssid": _ssid,
        "isVerified": true,
        "versionCode": 99,
        "androidID": (identifier != null ? identifier : ""),
        "IMEINo": myIP,
        "faId": sharedPreferences.getInt(CommonSharedPreferance.FA_ID)
      };
      var response = await http.post(
        Uri.parse(CommonConstants.HOST_URL + CommonConstants.CHECK_OUT_ATT_API),
        body: json.encode(datas),
        headers: CommonConstants.HOST_HEADER,
      );

//      var connectivityResult = await (Connectivity().checkConnectivity());
//      print(connectivityResult.toString());
//      // I am connected to a wifi network.
//      var wname = await (Connectivity().getWifiName());
//      var wip = await (Connectivity().getWifiIP());
//      print("Wifi IP : " + wip.toString());
//      print("Wifi Name : " + wname.toString());
//      print("BSSID Name : ");
      var datass = jsonDecode(response.body);
      bool isEarly = datass["isEarly"];
      if (isEarly) {
        earlyOutDialog(context);
      } else if (datass["outTime"] == null) {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text("OUT Attendance Not Marked")));
        outTime = "NULL";
      } else {
        sharedPreferences.setString(
            CommonSharedPreferance.OUT_TIME, datass["outTime"]);
        outTime = datass["outTime"];

        successOutDialog(context);
      }

      return "Success";
    }

    final List<String> _listViewData = [
      "Timetable",
      "Daily Task",
      "Summary",
      "Change password",
    ];
    var griViews = Center(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  ListTile(
                    dense: true,
                    leading: Icon(Icons.av_timer, color: Colors.amberAccent),

                    title: Text(
                      'Attendance\nSummary',
                      textAlign: TextAlign.center,
                    ),
                    //     subtitle: Text('Staff Attendance\nSummary', textAlign: TextAlign.center,),
                    onTap: () {
                      var route = new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new AttendanceSummary(),
                      );
                      //   Navigator.of(context).pop();
                      Navigator.of(context).push(route);
                    },
                  )
                ]),
              ),
            ),
          ),
          Expanded(
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  ListTile(
                    dense: true,
                    leading: Icon(Icons.perm_contact_calendar,
                        color: Colors.black54),

                    title: Text(
                      'User\nProfile',
                      textAlign: TextAlign.center,
                    ),
                    //       subtitle: Text('Staff user \nProfile', textAlign: TextAlign.center,),
                    onTap: () {
                      getData();
                    },
                  )
                ]),
              ),
            ),
          ),
        ],
      ),
    );
    Widget griViews1() {
      int role = sharedPreferences.getInt(CommonSharedPreferance.ROLE) == null
          ? 1
          : sharedPreferences.getInt(CommonSharedPreferance.ROLE);

      if (role != 3) {
        return SizedBox(
          height: 5.0,
        );
      }
      return Center(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child:
                      Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    ListTile(
                      dense: true,
                      leading: Icon(Icons.person_pin, color: Colors.green),

                      title: Text(
                        'Staff\nAttendance',
                        textAlign: TextAlign.center,
                      ),
                      //        subtitle: Text('Staff Present\n Duty-Leave', textAlign: TextAlign.center,),
                      onTap: () {
                        var route = new MaterialPageRoute(
                          builder: (BuildContext context) => new HomeView(),
                        );
                        //     Navigator.of(context).pop();
                        Navigator.of(context).push(route);
                      },
                    )
                  ]),
                ),
              ),
            ),
            Expanded(
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child:
                      Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    ListTile(
                      dense: true,
                      leading: Icon(Icons.edit, color: Colors.blueGrey),
                      title: Text(
                        'Edit\nAction',
                        textAlign: TextAlign.center,
                      ),
                      onTap: () {
                        var route = new MaterialPageRoute(
                          builder: (BuildContext context) => new HomeEditView(),
                        );
                        //      Navigator.of(context).pop();
                        Navigator.of(context).push(route);
                      },
                    )
                  ]),
                ),
              ),
            ),
          ],
        ),
      );
    }

    var griViews2 = Center(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  Center(
                    child: ListTile(
                      dense: true,
                      leading: Icon(Icons.title, color: Colors.deepPurple),
                      title: Text(
                        'Daily\nTask',
                        textAlign: TextAlign.center,
                      ),
                      onTap: () {
                        var route = new MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new DailyTaskView(),
                        );
                        //     Navigator.of(context).pop();
                        Navigator.of(context).push(route);
                      },
                    ),
                  ),
                ]),
              ),
            ),
          ),
          Expanded(
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  ListTile(
                    dense: true,
                    leading: Icon(Icons.add_alert, color: Colors.red),
                    title: Text(
                      'Staff\nNotices',
                      textAlign: TextAlign.center,
                    ),
                    onTap: () {
                      responseForNotice();
                    },
                  )
                ]),
              ),
            ),
          ),
        ],
      ),
    );
    Widget inAttV(String data) {
      return Card(
        child: Center(
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  ListTile(
                    dense: true,
                    leading: Icon(Icons.exit_to_app),
                    title: Text('IN Attendance'),
                    subtitle: Text(' Date : ' +
                        DateTime.now().day.toString() +
                        "/" +
                        DateTime.now().month.toString() +
                        "/" +
                        DateTime.now().year.toString() +
                        "\n Time : " +
                        data),
                  )
                ]),
              ),
              Expanded(
                flex: 1,
                child: ButtonBar(
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        'Mark In',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'serif'),
                      ),
                      onPressed: () {
                        CircularProgressIndicator();
//                    fetchAll();
                        getInAtt(context);
                      },
                      color: Colors.indigo,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }

    var inAtt = Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              dense: true,
              leading: Icon(Icons.touch_app),
              title: Text('IN Attendance!'),
              subtitle: Text(' Date : ' +
                  DateTime.now().day.toString() +
                  "/" +
                  DateTime.now().month.toString() +
                  "/" +
                  DateTime.now().year.toString() +
                  "\n Time : " +
                  inTime),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: Text(
                    'Mark In',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'serif'),
                  ),
                  onPressed: () {
                    CircularProgressIndicator();
//                      fetchAll();
                    getInAtt(context);
                  },
                  color: Colors.indigo,
                ),
              ],
            ),
          ],
        ),
      ),
    );
    Widget outAttV(String data) {
      return Card(
        child: Center(
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  ListTile(
                    dense: true,
                    leading: Icon(Icons.open_in_new),
                    title: Text('OUT Attendance'),
                    subtitle: Text(' Date : ' +
                        DateTime.now().day.toString() +
                        "/" +
                        DateTime.now().month.toString() +
                        "/" +
                        DateTime.now().year.toString() +
                        "\n Time : " +
                        outTime),
                  )
                ]),
              ),
              Expanded(
                flex: 1,
                child: ButtonBar(
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        'Mark Out',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'serif'),
                      ),
                      onPressed: () {
                        CircularProgressIndicator();
//                      fetchAll();
                        getOutAtt(context);
                      },
                      color: Colors.indigo,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }

    var outAtt = Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
                leading: Icon(Icons.add_to_home_screen),
                dense: true,
                title: Text('Out Attendance!'),
                subtitle: Text(' Date : ' +
                    DateTime.now().day.toString() +
                    "/" +
                    DateTime.now().month.toString() +
                    "/" +
                    DateTime.now().year.toString() +
                    "\n Time : " +
                    outTime)),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: Text(
                    'Mark Out',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'serif'),
                  ),
                  onPressed: () {
                    CircularProgressIndicator();
//                      fetchAll();
                    getOutAtt(context);
                  },
                  color: Colors.indigo,
                ),
              ],
            ),
          ],
        ),
      ),
    );

    checkSharedPrefernce();
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: key,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text("MAS Dashboard"),
        ),
        body: FutureBuilder(
            future: checkSharedPrefernce(),
            builder: (context, snapshot) {
              return snapshot.data == null
                  ? Center(child: CircularProgressIndicator())
                  : SingleChildScrollView(
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        padding: EdgeInsets.all(16),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            StreamBuilder<String>(
                                builder: (context, snapshot) => Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          StreamBuilder<String>(
                                            builder: (context, snapshot) =>
                                                Image(
                                              height: 70.0,
                                              width: 70.0,
                                              semanticLabel:
                                                  "Mobile Attendance System",
                                              image: AssetImage(
                                                  "assets/image/seals.png"),
                                            ),
                                          ),
                                          StreamBuilder<String>(
                                            builder: (context, snapshot) =>
                                                Text(
                                              "Education Department\nGovernment of Gujarat\n(Mobile Attendance System)",
                                              style: TextStyle(
                                                  fontSize: 10.0,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'serif'),
                                            ),
                                          ),
                                        ])),
                            StreamBuilder<String>(
                              builder: (context, snapshot) => Image(
                                height: 70.0,
                                width: 70.0,
                                semanticLabel: "Mobile Attendance System",
                                image: AssetImage("assets/image/proffes.png"),
                              ),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            StreamBuilder<String>(
                              builder: (context, snapshot) => Text(
                                "Institute Name : " +
                                    instName +
                                    "\n Name : " +
                                    facName,
                                style: TextStyle(
                                    fontSize: 10.0,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'serif'),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            inAttV(inTime),
                            SizedBox(
                              height: 5.0,
                            ),
                            griViews,
                            SizedBox(
                              height: 5.0,
                            ),
                            griViews1(),
                            SizedBox(
                              height: 5.0,
                            ),
                            griViews2,
                            SizedBox(
                              height: 5.0,
                            ),
                            outAttV(outTime),
                            SizedBox(
                              height: 10.0,
                            ),
                            InkWell(
                                child: new Text(
                                  'Change Password',
                                  style: TextStyle(
                                      color: Colors.black,
                                      decoration: TextDecoration.underline,
                                      decorationColor: Colors.red,
                                      fontSize: 18),
                                ),
                                onTap: () {
                                  var route = new MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        new MyChangePassword(),
                                  );
                                  //      Navigator.of(context).pop();
                                  Navigator.of(context).push(route);
                                }),
                            SizedBox(
                              height: 15.0,
                            ),
                            StreamBuilder<String>(
                              builder: (context, snapshot) =>
                                  Text("DTE Gujarat @ 2020"),
                            ),
                          ],
                        ),
                      ),
                    );
            }),
      ),
    );
  }

  responseForNotice() {
    print("CALLING VALUE");
    int role = sharedPreferences.getInt(CommonSharedPreferance.ROLE) == null
        ? 1
        : sharedPreferences.getInt(CommonSharedPreferance.ROLE);

    if (role == 3) {
      var route = new MaterialPageRoute(
        builder: (BuildContext context) => new PrincipalStaffNotice(),
      );
      //      Navigator.of(context).pop();
      Navigator.of(context).push(route);
    } else {
      print("CALLING VALUE 2");
      var route = new MaterialPageRoute(
        builder: (BuildContext context) => new StaffNotice(),
      );
      //      Navigator.of(context).pop();
      Navigator.of(context).push(route);
    }
  }

  @override
  // ignore: must_call_super
  void initState() {
    checkSharedPrefernce();
  }
}
