import 'dart:convert';

import 'package:f_app/LecLabDetails.dart';
import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Trip.dart';
import 'DailyTaskDetails.dart';
class DailyTaskView extends StatefulWidget {
  @override
  _MyDailyTaskView createState() => _MyDailyTaskView();
}

final key = new GlobalKey<ScaffoldState>();

class _MyDailyTaskView extends State<DailyTaskView> {
  List<Map> leaveDutyType = [];
  List<DailyTask> dailyTaskList = [
    DailyTask("Slot - 1", "not submitted", null, null, null, null, null, 1),
    DailyTask("Slot - 2", "not submitted", null, null, null, null, null, 2),
    DailyTask("Slot - 3", "not submitted", null, null, null, null, null, 3),
    DailyTask("Slot - 4", "not submitted", null, null, null, null, null, 4),
    DailyTask("Slot - 5", "not submitted", null, null, null, null, null, 5),
    DailyTask("Slot - 6", "not submitted", null, null, null, null, null, 6),
    DailyTask("Slot - 7", "not submitted", null, null, null, null, null, 7),
    DailyTask("Slot - 8", "not submitted", null, null, null, null, null, 8),
  ];

  int _sNumber = 1;
  final List<Notice> facultyList = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;

  // List<Map> status = [];

  Future<List<DailyTask>> getData() async {
    sharedPreferences = await SharedPreferences.getInstance();
    print("Getting Data");
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String today = formatter.format(now);

    var datas = {
      "facultyId": sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
      "dayDate": today
    };

    var response = await http.post(
      Uri.parse(CommonConstants.HOST_URL + CommonConstants.FAC_ACT_LIST_API),
      body: json.encode(datas),
      headers: CommonConstants.HOST_HEADER,
    );
    var data = jsonDecode(response.body);
    var dlist = data as List;
    // final dlist = ddmap["content"] as List;
    print("New Data : " + data.toString());

    int cnt = 1;
    for (var name in dlist) {
      var dmap = name as Map;
      //  int semId = dmap["semId"];
      int slotId = dmap["slotId"];
      String msg = dmap["message"];

      dailyTaskList[slotId - 1].title = (msg != null ? msg : "not submitted");
      dailyTaskList[slotId - 1].subjectName = " ";

//      facultyList.add(Notice(facName, deptName, reason, memoDate, dMsg,
//          memoCount, remark, memoId));
      cnt += 1;
    }

    Map mp = {"id": 1, "name": "Del. Lecture"};
    leaveDutyType.add(mp);
    mp = {"id": 2, "name": "Conducting Lab"};
    leaveDutyType.add(mp);
    mp = {"id": 3, "name": "Tutorial"};
    leaveDutyType.add(mp);
    mp = {"id": 4, "name": "Finishing School"};
    leaveDutyType.add(mp);
    mp = {"id": 5, "name": "Lab. setup"};
    leaveDutyType.add(mp);
    mp = {"id": 6, "name": "Lecture Preparation"};
    leaveDutyType.add(mp);
    mp = {"id": 7, "name": "Student Mentor / Counselling"};
    leaveDutyType.add(mp);
    mp = {"id": 8, "name": "Dept. Admin Work"};
    leaveDutyType.add(mp);
    mp = {"id": 9, "name": "Inst. Admin Work"};
    leaveDutyType.add(mp);
    mp = {"id": 10, "name": "NSS Activity"};
    leaveDutyType.add(mp);
    mp = {"id": 11, "name": "NCC Activity"};
    leaveDutyType.add(mp);
    mp = {"id": 12, "name": "Exam Work"};
    leaveDutyType.add(mp);
    mp = {"id": 13, "name": "Research Work"};
    leaveDutyType.add(mp);
    mp = {"id": 14, "name": "Involved in CTE-CHE/Univ/KCG Work"};
    leaveDutyType.add(mp);
    mp = {"id": 15, "name": "Other Work"};
    leaveDutyType.add(mp);

    return dailyTaskList;
  }

  String _mySelection;
  String _mySelectionString;

  String getSelectedValue(int value) {
    String message = "<<Select data>>";
    for (Map test in leaveDutyType) {
      if (test["id"] == value) {
        message = test["name"];
        break;
      }
    }
    return message;
  }

  @override
  Widget build(BuildContext context) {
    //getData();
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white70,
      appBar: AppBar(
        title: Text("Dailty Task"),
      ),
      body: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            return snapshot.data != null
                ? buildTripCard(snapshot.data)
                : Center(child: CircularProgressIndicator());
          }),
    );
//      Container(
//
//      child: new ListView.builder(
//          itemCount: tripsList.length,
//          itemBuilder: (BuildContext context, int index) =>
//              buildTripCard(context, index)),
//      color: Colors.white70,
//      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
//    );
  }

  Widget buildTripCard(List<DailyTask> dailyTaskDetails) {
    if (dailyTaskDetails.length == 0) {
      return new Container(
          child: Center(
        child: Text(
          "No Data Found..",
          style: new TextStyle(fontSize: 22.0),
        ),
      ));
    } else {
      return new Container(
        child: ListView.builder(
            itemCount: dailyTaskDetails.length,
            padding: const EdgeInsets.all(2.0),
            itemBuilder: (context, position) {
              return Card(
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 1.0, bottom: 1.0),
                        child: Row(children: <Widget>[
                          Text(
                            dailyTaskDetails[position].slotNo,
                            style: new TextStyle(fontSize: 16.0),
                          ),
                          Spacer(),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
                        child: Row(children: <Widget>[
                          Text(
                            dailyTaskDetails[position].title,
                            style: new TextStyle(fontSize: 21.0),
                          ),
                          Spacer(),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 1.0, bottom: 1.0),
                        child: Row(
                          children: <Widget>[
                            Spacer(),
                            ButtonBar(
                              children: <Widget>[
                                FlatButton(
                                  child: Text(
                                    dailyTaskDetails[position].title ==
                                            "not submitted"
                                        ? "ADD"
                                        : "VIEW",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'serif'),
                                  ),
                                  onPressed: () {
                                    if (dailyTaskDetails[position].title ==
                                        "not submitted") {
                                      showDialog(
                                        context: context,
                                        barrierDismissible: false,
                                        child: new AlertDialog(
                                          title: new Text(
                                            "Daily Task Activity",
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'serif'),
                                          ),
                                          content: DropdownButton<String>(
                                            isDense: true,
                                            isExpanded: true,
                                            hint: new Text("<< Select >>"),
                                            value: _mySelection != null
                                                ? _mySelection
                                                : null,
                                            onChanged: (String newValue) {
                                              Navigator.of(context,
                                                      rootNavigator: true)
                                                  .pop('dialog');

//                                                setState(() {
//                                                  _mySelection = newValue;
//                                                });
//
//
                                              _mySelection = newValue;
                                              _mySelectionString =
                                                  getSelectedValue(
                                                      int.parse(_mySelection));
                                              showDialog(
                                                context: context,
                                                barrierDismissible: false,
                                                child: new AlertDialog(
                                                  title: new Text(
                                                    "Daily Task Action",
                                                    style: TextStyle(
                                                        color: Colors.red,
                                                        fontSize: 15.0,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily: 'serif'),
                                                  ),
                                                  content: Text(
                                                      "Your Selected Task is \" $_mySelectionString\" ."),
                                                  actions: <Widget>[
                                                    ButtonBar(
                                                      children: <Widget>[
                                                        new FlatButton(
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context,
                                                                    rootNavigator:
                                                                        true)
                                                                .pop('dialog');
                                                            _mySelection = null;
                                                            _mySelectionString =
                                                                null;
                                                          },
                                                          child: new Text(
                                                            'No',
                                                            style: TextStyle(
                                                                color:
                                                                    Colors.red,
                                                                fontSize: 15.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontFamily:
                                                                    'serif'),
                                                          ),
                                                        ),
                                                        FlatButton(
                                                          child: Text(
                                                            'Submit',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .green,
                                                                fontSize: 15.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontFamily:
                                                                    'serif'),
                                                          ),
                                                          onPressed: () {
                                                            _sNumber =
                                                                dailyTaskDetails[
                                                                        position]
                                                                    .slotId;
                                                            performEditOperation();
                                                            Navigator.of(
                                                                    context,
                                                                    rootNavigator:
                                                                        true)
                                                                .pop('dialog');
                                                          },
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              );
//
                                            },
                                            items: leaveDutyType.map((Map map) {
                                              return new DropdownMenuItem<
                                                  String>(
                                                value: map["id"].toString(),
                                                child: new Text(
                                                  map["name"],
                                                ),
                                              );
                                            }).toList(),
                                          ),
                                          actions: <Widget>[
                                            ButtonBar(
                                              children: <Widget>[
                                                FlatButton(
                                                  child: Text(
                                                    'Exit',
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 15.0,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily: 'serif'),
                                                  ),
                                                  onPressed: () {
                                                    Navigator.of(context,
                                                            rootNavigator: true)
                                                        .pop('dialog');
                                                    _mySelection = null;
                                                    _mySelectionString = null;
                                                  },
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      );
                                    } else {
                                      _sNumber =
                                          dailyTaskDetails[
                                          position]
                                              .slotId;
                                      getTaskDetailsView(context, dailyTaskDetails[position].title, _sNumber);
                                    }
                                    print("Clicked!");
                                    // show(context, faculty[position]);
                                  },
                                  color: Colors.indigo,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
      );
    }
  }

  Future<String> performEditOperation() async {
    sharedPreferences = await SharedPreferences.getInstance();
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String today = formatter.format(now);
    print("DATA : " + _mySelection.toString());
    var bdata = {
      "facultyId": sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
      "slotId": _sNumber,
      "dayId": 1,
      "message": _mySelectionString,
      "semId": "",
      "subjectId": "",
    };
    var response = await http.post(
      Uri.parse(CommonConstants.HOST_URL + CommonConstants.FAC_ACT_UPDATE_API),
      body: json.encode(bdata),
      headers: CommonConstants.HOST_HEADER,
    );

    var datas = jsonDecode(response.body);
    print("Response : " + datas.toString());

    //print("Datass : " + leaveDutyType.toString());

    if (_mySelectionString == "Del. Lecture" ||
        _mySelectionString == "Conducting Lab" ||
        _mySelectionString == "Tutorial") {
      var route = new MaterialPageRoute(
        builder: (BuildContext context) =>
            new LecLabDetails(_mySelectionString, _sNumber),
      );
      // Navigator.of(context).pop();
      Navigator.of(context).push(route);
    } else {
      _mySelection = null;
      _mySelectionString = null;
      setState(() {});
    }
    _mySelection = null;
    _mySelectionString = null;
    return "Success";
  }


  getTaskDetailsView(context,String title, int slotNo) async
  {
      if (title == "Del. Lecture" ||
          title == "Conducting Lab" ||
          title == "Tutorial") {

        var bdata = {
          "facultyId": sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
          "slotId": slotNo,

        };
        var response = await http.post(
          Uri.parse(CommonConstants.HOST_URL + CommonConstants.STD_ATT_DETAILS_API),
          body: json.encode(bdata),
          headers: CommonConstants.HOST_HEADER,
        );

        var  datas = jsonDecode(response.body);
        print("Lec/Lab Response : " + datas.toString());

        var route = new MaterialPageRoute(
          builder: (BuildContext context) =>
          new DailyTaskViewDetails(datas),
        );
        // Navigator.of(context).pop();
        Navigator.of(context).push(route);

      }
      else {
        _scaffoldKey.currentState
            .showSnackBar(new SnackBar(
          content: new Text("Task : " +
              title),
        ));
      }
  }

//
//  jsonObject.accumulate("facultyId", UID);
//  jsonObject.accumulate("slotId", CSlot_id);
//  STD_ATT_DETAILS_API
//
//  if (_mySelectionString == "Del. Lecture" ||
//  _mySelectionString == "Conducting Lab" ||
//  _mySelectionString == "Tutorial") {

//  Future<bool> _onWillPop() async {
//    showDialog(
//      context: context,
//      builder: (context) => new AlertDialog(
//        title: new Text('Are you sure?'),
//        content: new Text('Do you want to submit task'),
//        actions: <Widget>[
//          new FlatButton(
//            onPressed: () => Navigator.of(context).pop(false),
//            child: new Text('No'),
//          ),
//          new FlatButton(
//            onPressed: () => performEditOperation(),
//            child: new Text('Yes'),
//          ),
//        ],
//      ),
//    );
//  }

//  show(BuildContext context, Notice data) {
//    print("Clicked Process!");
//    var route = new MaterialPageRoute(
//      builder: (BuildContext context) => new StaffNoticeDetails(data),
//    );
//    // Navigator.of(context).pop();
//    Navigator.of(context).push(route);
//  }
}
