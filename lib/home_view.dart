import 'dart:convert';

import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Trip.dart';
import 'dashboard.dart';

class HomeView extends StatefulWidget {
  //   final List<Trip> tripsList = [
  //    Trip("Bhavya shah", DateTime.now(), DateTime.now(), 200, "C.E"),
  //    Trip("Hemant Joshi", DateTime.now(), DateTime.now(), 450, "Civil"),
  //    Trip("Dhaval Sathvara", DateTime.now(), DateTime.now(), 900, "Admin"),
  //    Trip("Parth Modi", DateTime.now(), DateTime.now(), 170, "I.C"),
  //    Trip("Tushar Raval", DateTime.now(), DateTime.now(), 180, "C.E"),
  //  ];

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  List<Trip> tripsList = [];
  MyGlobals myGlobals = new MyGlobals();
  SharedPreferences sharedPreferences;
  DateTime _dateTime;
  int cnt = 0;
  // List<LeaveDutyType> leaveDutyType = [];

  static var now = new DateTime.now();
  static var formatter = new DateFormat('yyyy-MM-dd');

  List<Map> leaveDutyType = [];
  String _value = formatter.format(now);

  Future<List<Trip>> getData() async {
    sharedPreferences = await SharedPreferences.getInstance();

    if (tripsList.length < 1) {
      var response = await http.get(
        Uri.parse(CommonConstants.HOST_URL +
            CommonConstants.FAC_LIST_API +
            sharedPreferences
                .getInt(CommonSharedPreferance.INSTITUTE_ID)
                .toString()),
        headers: CommonConstants.HOST_HEADER,
      );

      var data = jsonDecode(response.body);
      final dlist = data as List;
      // final dlist = ddmap["content"] as List;
      print("Data : " + dlist.toString());

      int cnt = 1;
      for (var name in dlist) {
        final dmap = name as Map;
        int fId = dmap["id"];
        String fName = dmap["fName"];
        String departmentName = dmap["departmentName"];

        tripsList.add(
            Trip(fName, DateTime.now(), DateTime.now(), fId, departmentName));
        cnt += 1;
      }

      response = await http.get(
        Uri.parse(CommonConstants.HOST_URL + CommonConstants.LEAVETYPE_API),
        headers: CommonConstants.HOST_HEADER,
      );

      data = jsonDecode(response.body);
      final Typedlist = data as List;

      Map mp = {"id": 99, "name": "By Principal"};
      leaveDutyType.add(mp);
      cnt = 1;
      for (var names in Typedlist) {
        final dmap = names as Map;
        // final dlist = ddmap["content"] as List;
        //  print("Data : " + Typedlist.toString());
        int id = dmap["id"];
        String name = dmap["name"];
        Map mp = {"id": id, "name": name};
        leaveDutyType.add(mp);
      }
      //print("Datass : " + leaveDutyType.toString());

    }
    return tripsList;
  }

//  void _onTapItem(BuildContext context, Trip trip) {
//    Navigator.of(context).push(MaterialPageRoute(
//        builder: (BuildContext context) => NewsDetails(trip, "Mark Staff Attendance")));
//  }

  String _mySelection;
  String _mySelectionString;

  String getSelectedValue(int value) {
    String message = "<<Select data>>";
    for (Map test in leaveDutyType) {
      if (test["id"] == value) {
        message = test["name"];
        break;
      }
    }
    return message;
  }

  @override
  Widget build(BuildContext context) {
    //getData();
    return Scaffold(
      backgroundColor: Colors.white70,
      appBar: AppBar(
        title: Text("Mark Staff Attendance"),
      ),
      body: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            return snapshot.data != null
                ? buildTripCard(snapshot.data)
                : Center(child: CircularProgressIndicator());
          }),
    );
//      Container(
//
//      child: new ListView.builder(
//          itemCount: tripsList.length,
//          itemBuilder: (BuildContext context, int index) =>
//              buildTripCard(context, index)),
//      color: Colors.white70,
//      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
//    );
  }

  Widget buildTripCard(List<Trip> trip) {
    if (trip.length == 0) {
      return new Container(
          child: Center(
        child: Text(
          "No Data Found..",
          style: new TextStyle(fontSize: 22.0),
        ),
      ));
    } else {
      String dropdownValue = 'One';
      var now = new DateTime.now();
      var formatter = new DateFormat('yyyy-MM-dd');
      String today = formatter.format(now);
      return new Container(
        child: ListView.builder(
            itemCount: trip.length,
            padding: const EdgeInsets.all(2.0),
            itemBuilder: (context, position) {
              return Card(
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 1.0, bottom: 1.0),
                        child: Row(children: <Widget>[
                          Text(
                            trip[position].title,
                            style: new TextStyle(fontSize: 22.0),
                          ),
                          Spacer(),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, bottom: 5.0),
                        child: Row(children: <Widget>[
                          Text(trip[position].travelType),
                          Spacer(),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 1.0, bottom: 1.0),
                        child: Row(
                          children: <Widget>[
                            Spacer(),
                            ButtonBar(
                              children: <Widget>[
                                FlatButton(
                                  child: Text(
                                    'Action',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'serif'),
                                  ),
                                  onPressed: () {
                                    int fId = trip[position].budget;
                                    String fName = trip[position].title;
                                    String fDepartment =
                                        trip[position].travelType;
                                    showDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      child: new AlertDialog(
                                        title: new Text(
                                          "Staff Action",
                                          style: TextStyle(
                                              color: Colors.red,
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'serif'),
                                        ),
                                        content: DropdownButton<String>(
                                          isDense: true,
                                          isExpanded: true,
                                          hint: new Text("<< Select >>"),
                                          value: _mySelection != null
                                              ? _mySelection
                                              : null,
                                          onChanged: (String newValue) {
                                            Navigator.of(context,
                                                    rootNavigator: true)
                                                .pop('dialog');

//                                                setState(() {
//                                                  _mySelection = newValue;
//                                                });
//
//
                                            _mySelection = newValue;
                                            _mySelectionString =
                                                getSelectedValue(
                                                    int.parse(_mySelection));
                                            showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              child: new AlertDialog(
                                                title: new Text(
                                                  "Staff Action",
                                                  style: TextStyle(
                                                      color: Colors.red,
                                                      fontSize: 15.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontFamily: 'serif'),
                                                ),
                                                content: Column(
                                                  children: <Widget>[
                                                    Text(
                                                        "Your Selected Action is \" $_mySelectionString\" for $fName of $fDepartment Department."),
                                                    SizedBox(
                                                      height: 5.0,
                                                    ),
                                                    Text("From Date : $today"),
                                                    SizedBox(
                                                      height: 5.0,
                                                    ),
                                                    new Text("To Date : $cnt" +
                                                        (_dateTime != null
                                                            ? formatter.format(
                                                                _dateTime)
                                                            : formatter
                                                                .format(now))),
                                                    new RaisedButton(
                                                      onPressed: () {
                                                        showDatePicker(
                                                                context:
                                                                    context,
                                                                initialDate:
                                                                    _dateTime !=
                                                                            null
                                                                        ? _dateTime
                                                                        : now,
                                                                firstDate: new DateTime(
                                                                    now.year,
                                                                    now.month,
                                                                    now.day),
                                                                lastDate:
                                                                    new DateTime(
                                                                        now.year +
                                                                            1))
                                                            .then((dates) {
                                                          setState(() {
                                                            _dateTime = dates;
                                                            cnt += 1;
                                                          });
                                                        });
                                                        print("To Date : " +
                                                            _dateTime
                                                                .toString());
                                                      },
                                                      child: new Text(
                                                          'Change ToDate'),
                                                    ),
                                                    SizedBox(
                                                      height: 5.0,
                                                    ),
                                                    new TextFormField(
                                                      decoration:
                                                          const InputDecoration(
                                                        icon: const Icon(
                                                            Icons.subject),
                                                        hintText:
                                                            'Enter Reason',
                                                        labelText: 'Reason',
                                                      ),
                                                      inputFormatters: [
                                                        new LengthLimitingTextInputFormatter(
                                                            30)
                                                      ],
                                                      // validator: (val) =>
                                                      //   val.isEmpty ? 'Subject Name is required' : null,
                                                      //onSaved: (val) => labDetailsClass.subjectName = val,
                                                    ),
                                                  ],
                                                ),
                                                actions: <Widget>[
                                                  ButtonBar(
                                                    children: <Widget>[
                                                      new FlatButton(
                                                        onPressed: () {
                                                          Navigator.of(context,
                                                                  rootNavigator:
                                                                      true)
                                                              .pop('dialog');
                                                          _mySelection = null;
                                                          _mySelectionString =
                                                              null;
                                                        },
                                                        child: new Text(
                                                          'No',
                                                          style: TextStyle(
                                                              color: Colors.red,
                                                              fontSize: 15.0,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontFamily:
                                                                  'serif'),
                                                        ),
                                                      ),
                                                      FlatButton(
                                                        child: Text(
                                                          'Submit',
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.green,
                                                              fontSize: 15.0,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontFamily:
                                                                  'serif'),
                                                        ),
                                                        onPressed: () {
                                                          performEditOperation(
                                                              context, fId);
                                                          Navigator.of(context,
                                                                  rootNavigator:
                                                                      true)
                                                              .pop('dialog');
                                                        },
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                            );
//
                                          },
                                          items: leaveDutyType.map((Map map) {
                                            return new DropdownMenuItem<String>(
                                              value: map["id"].toString(),
                                              child: new Text(
                                                map["name"],
                                              ),
                                            );
                                          }).toList(),
                                        ),
                                        actions: <Widget>[
                                          ButtonBar(
                                            children: <Widget>[
                                              FlatButton(
                                                child: Text(
                                                  'Exit',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 15.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontFamily: 'serif'),
                                                ),
                                                onPressed: () {
                                                  Navigator.of(context,
                                                          rootNavigator: true)
                                                      .pop('dialog');
                                                  _mySelection = null;
                                                  _mySelectionString = null;
                                                },
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                  color: Colors.indigo,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
      );
    }
  }

  Future<String> performEditOperation(context, int fId) async {
    sharedPreferences = await SharedPreferences.getInstance();
    print("CALLING  Submit Function!");

    String today = formatter.format(now);
    print("DATA : " + today);
    try {
      var bdata = {
        "duration": 1,
        "endDate": _value,
        "facultyIds": [fId],
        "instituteId":
            sharedPreferences.getInt(CommonSharedPreferance.INSTITUTE_ID),
        "reasonByPrincipal": _mySelectionString,
        "startDate": today,
        "typeId": int.parse(_mySelection)
      };
      var response = await http.post(
        Uri.parse(CommonConstants.HOST_URL + CommonConstants.FAC_ATT_BYPRI_API),
        body: json.encode(bdata),
        headers: CommonConstants.HOST_HEADER,
      );

      _mySelection = null;
      _mySelectionString = null;
      var datass = jsonDecode(response.body);
      print(datass.toString());
    } on Exception catch (_) {
      print('never reached');
    } finally {
      var route = new MaterialPageRoute(
        builder: (BuildContext context) => new HomeView(),
      );
      Navigator.of(context).pop();
      Navigator.of(context).push(route);
    }
    return "Success";

    return "Success";
  }

  void summaryTabClick(context) {
    showDialog(
        barrierDismissible: false,
        context: myGlobals.scaffoldKey.currentContext,
        child: new AlertDialog(
            title: new Text(
              "Summary Tab Clicked!",
              style: TextStyle(
                  color: Colors.green,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'serif'),
            ),
            actions: <Widget>[
              ButtonBar(
                children: <Widget>[
                  FlatButton(
                    child: Text(
                      'Thank You',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'serif'),
                    ),
                    onPressed: () {
                      print("Click Button!");
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                    },
                    color: Colors.red,
                  ),
                ],
              )
            ]));
  }
}
