import 'dart:convert';

import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AttendanceSummary extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<AttendanceSummary> {
  String dataHeadingInfo = "Date, IN Time, OUT Time, Status";
  String dataDay1 = "2020-01-11, 10:20:15, 18:12:17, Present";
  String dataDay2 = "2020-01-10, #, #, Weekend";
  String dataDay3 = "2020-01-09, #, #, Duty";
  String dataDay4 = "2020-01-08, #, #, Holiday";
  String dataDay5 = "2020-01-07, #, #, Leave";
  String dataDay6 = "2020-01-06, 1:45:22, #, Late-IN";
  SharedPreferences sharedPreferences;

  Future<String> getData() async {
    sharedPreferences = await SharedPreferences.getInstance();
    var now = new DateTime.now();
    var start = new DateTime(now.year, now.month, now.day - 6);
    var formatter = new DateFormat('yyyy-MM-dd');
    String endDate = formatter.format(now);
    String startDate = formatter.format(start);
    var datas = {
      "facultyId": sharedPreferences.getInt(CommonSharedPreferance.FACULTY_ID),
      "startDate": startDate,
      "endDate": endDate,
      "page": 0,
      "limit": 1000
    };

    var response = await http.post(
      Uri.parse(CommonConstants.HOST_URL + CommonConstants.FAC_ATT_LIST_API),
      body: json.encode(datas),
      headers: CommonConstants.HOST_HEADER,
    );

    var data = jsonDecode(response.body);
    final ddmap = data as Map;
    final dlist = ddmap["content"] as List;
    print("Data : " + ddmap.toString());

    int cnt = 1;

    for (var name in dlist) {
      final dmap = name as Map;
      String attDate = dmap["attendanceDate"].substring(0, 10);
      String inTime = dmap["inTime"] != null ? dmap["inTime"] : "-";
      String outTime = dmap["outTime"] != null ? dmap["outTime"] : "-";
      String status = "Status";

      bool isLate = dmap["isLate"];
      bool isEarly = dmap["isEarly"];
      bool presenceStatus = dmap["presenceStatus"];
      int leaveStatus = dmap["leaveStatus"];

      if (presenceStatus) {
        if (isLate == true) {
          status = "Late IN";
        } else if (isEarly == true) {
          status = "Early Out";
        } else if (leaveStatus == 2) {
          status = "By Principal";
        } else if (leaveStatus == 1) {
          status = "Duty";
        } else if (attDate != startDate && outTime == "-") {
          status = "Out Missing";
        } else {
          status = "Present";
        }
      } else {
        if (leaveStatus == 0) {
          status = "Absent";
        } else if (leaveStatus == 1) {
          status = "Leave";
        } else if (leaveStatus == 2) {
          status = "Weekend";
        } else if (leaveStatus == 3) {
          status = "Holiday";
        } else if (leaveStatus == 4) {
          status = "Vacation";
        } else {
          status = "Other";
        }
      }

      if (cnt == 1) {
        dataDay1 = "$attDate, $inTime, $outTime, $status";
        print("Get Data! $dataDay1");
      }
      if (cnt == 2) {
        dataDay2 = "$attDate, $inTime, $outTime, $status";
        print("Get Data! $dataDay2");
      }
      if (cnt == 3) {
        dataDay3 = "$attDate, $inTime, $outTime, $status";
        print("Get Data! $dataDay3");
      }
      if (cnt == 4) {
        dataDay4 = "$attDate, $inTime, $outTime, $status";
        print("Get Data! $dataDay4");
      }
      if (cnt == 5) {
        dataDay5 = "$attDate, $inTime, $outTime, $status";
        print("Get Data! $dataDay5");
      }
      if (cnt == 6) {
        dataDay6 = "$attDate, $inTime, $outTime, $status";
        print("Get Data! $dataDay6");
      }
      cnt += 1;
    }

    return "Success";
  }

  @override
  Widget build(BuildContext context) {
    // getData();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Attendance Summary"),
      ),
      body: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            return snapshot.data != null
                ? Center(
                    child: _buildTableRowHeading(dataHeadingInfo, dataDay1,
                        dataDay2, dataDay3, dataDay4, dataDay5, dataDay6))
                : Center(child: CircularProgressIndicator());
          }),
    );
//    return Scaffold(
//      appBar: AppBar(title: Text("Attendance Summary")),
//
//      body: _buildTableRowHeading(dataHeadingInfo, dataDay1, dataDay2, dataDay3,
//          dataDay4, dataDay5, dataDay6),
//    );
  }

  Table _buildTableRowHeading(
      String listOfNames,
      String listOfNames1,
      String listOfNames2,
      String listOfNames3,
      String listOfNames4,
      String listOfNames5,
      String listOfNames6) {
    return Table(
      border: TableBorder(
        horizontalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        verticalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
      ),
      children: <TableRow>[
        TableRow(
          children:
//      children: listOfNames.split(',').map((name) {
//        return Container(
//          alignment: Alignment.center,
//          child: Text(name, style: TextStyle(fontSize: 20.0,fontStyle: FontStyle.italic)),
//          padding: EdgeInsets.all(4.0),
//
//        );
//      }).toList(),
              <Widget>[
            Container(
              width: double.infinity,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children:

                          // table header items

                          listOfNames.split(',').map((name) {
                        return Container(
                          width: 105,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blueAccent)),
                          alignment: Alignment.center,
                          child: Text(name,
                              style: TextStyle(
                                  fontSize: 14.0,
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.bold)),
                          padding: EdgeInsets.all(3.0),
                        );
                      }).toList(),
                    ),
                    Row(
                      children: listOfNames1.split(',').map((name) {
                        return Container(
                          width: 105,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blueAccent)),
                          alignment: Alignment.center,
                          child: Text(name, style: TextStyle(fontSize: 14.0)),
                          padding: EdgeInsets.all(3.0),
                        );
                      }).toList(),
                    ),
                    Row(
                      children: listOfNames2.split(',').map((name) {
                        return Container(
                          width: 105,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blueAccent)),
                          alignment: Alignment.center,
                          child: Text(name, style: TextStyle(fontSize: 14.0)),
                          padding: EdgeInsets.all(3.0),
                        );
                      }).toList(),
                    ),
                    Row(
                      children: listOfNames3.split(',').map((name) {
                        return Container(
                          width: 105,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blueAccent)),
                          alignment: Alignment.center,
                          child: Text(name, style: TextStyle(fontSize: 14.0)),
                          padding: EdgeInsets.all(3.0),
                        );
                      }).toList(),
                    ),
                    Row(
                      children: listOfNames4.split(',').map((name) {
                        return Container(
                          width: 105,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blueAccent)),
                          alignment: Alignment.center,
                          child: Text(name, style: TextStyle(fontSize: 14.0)),
                          padding: EdgeInsets.all(3.0),
                        );
                      }).toList(),
                    ),
                    Row(
                      children: listOfNames5.split(',').map((name) {
                        return Container(
                          width: 105,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blueAccent)),
                          alignment: Alignment.center,
                          child: Text(name, style: TextStyle(fontSize: 14.0)),
                          padding: EdgeInsets.all(3.0),
                        );
                      }).toList(),
                    ),
                    Row(
                      children: listOfNames6.split(',').map((name) {
                        return Container(
                          width: 105,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blueAccent)),
                          alignment: Alignment.center,
                          child: Text(name, style: TextStyle(fontSize: 14.0)),
                          padding: EdgeInsets.all(3.0),
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  @override
  void initState() {
    getData();
  }
}
