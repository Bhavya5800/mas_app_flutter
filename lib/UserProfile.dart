import 'dart:convert';
import 'dart:ui' as ui;

import 'package:f_app/EditUserProfile.dart';
import 'package:f_app/utils/CommonConstants.dart';
import 'package:f_app/utils/CommonSharedPreferance.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class MyUserProlfile extends StatefulWidget {
  final data;
  MyUserProlfile(this.data);
  @override
  _MyUserProlfileState createState() => new _MyUserProlfileState(data);
}

class _MyUserProlfileState extends State<MyUserProlfile> {
  final data;
  _MyUserProlfileState(this.data);
  SharedPreferences sharedPreferences;

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;
    final String imgUrl =
        'https://rememberingplaces.com/wp-content/themes/listingpro/assets/images/admin/avtar.jpg';

    return new Stack(
      children: <Widget>[
        new Container(
          color: Colors.blue,
        ),
        new Image.network(
          imgUrl,
          fit: BoxFit.fill,
        ),
        new BackdropFilter(
            filter: new ui.ImageFilter.blur(
              sigmaX: 6.0,
              sigmaY: 6.0,
            ),
            child: new Container(
              decoration: BoxDecoration(
                color: Colors.blue.withOpacity(0.9),
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
              ),
            )),
        new Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: new AppBar(
            title: new Text("User Profile"),
            centerTitle: false,
            elevation: 0.0,
            backgroundColor: Colors.transparent,
          ),
          backgroundColor: Colors.blue.withOpacity(0.9),
          body: SingleChildScrollView(
            child: new Center(
              child: new Column(
                children: <Widget>[
                  new SizedBox(
                    height: _height / 12,
                  ),
                  new CircleAvatar(
                    radius: _width < _height ? _width / 4 : _height / 4,
                    backgroundImage: NetworkImage(imgUrl),
                  ),
                  new SizedBox(
                    height: _height / 25.0,
                  ),
                  new Text(
                    'Staff Name : ' + data["name"].toString(),
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    'Email : ' + data["email"].toString(),
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    'Phone : ' + data["phone"].toString(),
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    'Institute Name: ' + data["instituteName"].toString(),
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    'Shift Time : ' + data["inTime"].toString(),
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  new Padding(
                      padding: new EdgeInsets.only(
                          top: _height / 30,
                          left: _width / 8,
                          right: _width / 8)),
                  new Text(
                    'Today Time : ' + data["actInTime"].toString(),
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  new Divider(
                    height: _height / 30,
                    color: Colors.white,
                  ),
                  new Row(
                    children: <Widget>[
                      rowCell(
                          data["roleName"].toString() != null
                              ? data["roleName"].toString()
                              : "-",
                          'Role'),
                      rowCell(
                          data["designationName"].toString(), 'Designation'),
                      rowCell(data["departmentName"].toString(), 'Department'),
                    ],
                  ),
                  new Divider(height: _height / 30, color: Colors.white),
                  new Padding(
                    padding: new EdgeInsets.only(
                        left: _width / 8, right: _width / 8),
                    child: new FlatButton(
                      onPressed: () {
                        getData(context);
                      },
                      child: new Container(
                          child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Icon(Icons.person),
                          new SizedBox(
                            width: _width / 30,
                          ),
                          new Text('EDIT')
                        ],
                      )),
                      color: Colors.blue[50],
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Future<String> getData(context) async {
    sharedPreferences = await SharedPreferences.getInstance();

    var response = await http.get(
      Uri.parse(CommonConstants.HOST_URL +
          CommonConstants.DEPT_BYPRI_API +
          sharedPreferences
              .getInt(CommonSharedPreferance.INSTITUTE_ID)
              .toString()),
      headers: CommonConstants.HOST_HEADER,
    );

    var deptData = jsonDecode(response.body);
    print("List Data : " + deptData.toString());
    //  List<String> deptList = deptData as List;

    //   print(deptData.toString());

    var route = new MaterialPageRoute(
      builder: (BuildContext context) => new EdituserProfile(data, deptData),
    );
    Navigator.of(context).push(route);

    return "Success";
  }

  Widget rowCell(String count, String type) => new Expanded(
          child: new Column(
        children: <Widget>[
          new Text(
            '$count',
            style: new TextStyle(color: Colors.white),
          ),
          new Text(type,
              style: new TextStyle(
                  color: Colors.white, fontWeight: FontWeight.normal))
        ],
      ));
}
